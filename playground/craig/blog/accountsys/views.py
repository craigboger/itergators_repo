from django.shortcuts import render

# Import class based views
from django.contrib.auth.forms import UserCreationForm
from django.views import generic

# Import helpful utilities
from django.urls import reverse_lazy

# Create your views here.
class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'