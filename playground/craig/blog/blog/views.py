from django.shortcuts import render

# Import class-based views.
from django.views.generic import ListView
from django.views.generic import DetailView  # For blog pages
from django.views.generic.edit import CreateView # For creating blog entry
from django.views.generic.edit import UpdateView # For editing blog entry
from django.views.generic.edit import DeleteView # For warning message before deleting post

# Import models to use in views.
from .models import Post

# Import other helpful utilities
from django.urls import reverse_lazy

# Create your views here.

# View to show block posts to page.
class BlogListView(ListView):
    # Use post model for the list.
    model = Post
    # Template to use with the view.
    template_name = 'home.html'

# View to make blog pages.
class BlogDetailedView(DetailView):
    model = Post
    template_name = 'post_detail.html'

# View to add blog post to database.
class BlogCreateView(CreateView):
    model = Post
    template_name = 'post_new.html'
    fields = ['title', 'author', 'body']

# View to update a blog post
class BlogUpdateView(UpdateView):
    model = Post
    template_name = 'post_edit.html'
    fields = ['title', 'body']

# View to show warning message before deleting post
class BlogDeleteView(DeleteView):
    model = Post
    template_name = 'post_delete.html'
    success_url = reverse_lazy('home')