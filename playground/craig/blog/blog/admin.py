from django.contrib import admin
# Import our models
from .models import Post

# Register your models here.
admin.site.register(Post)