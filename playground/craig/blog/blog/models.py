# blog/models.py

from django.db import models
from django.urls import reverse

# Create your models here.

# Model for blog post
class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        'auth.User',
        on_delete = models.CASCADE
    )
    body = models.TextField()

    # String method for display of model
    def __str__(self):
        return self.title
    
    #Page 144 of book, standard way to get abs URL to an object
    def get_absolute_url(self):
        # Reverse let's us reference an object by its URL template name
        return reverse('post_detail', args=[str(self.id)])