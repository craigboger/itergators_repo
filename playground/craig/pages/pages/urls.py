# pages/urls.py

# URL dispatcher
from django.urls import path
# Import view to display
from .views import HomePageView, AboutPageView

urlpatterns = [
    path('about/', AboutPageView.as_view(), name='about'),
    path('', HomePageView.as_view(), name='home'),
]
