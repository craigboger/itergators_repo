from django.test import TestCase
# Run simple tests to ensure functionality.
# Suitable when database is not in use.
from django.test import SimpleTestCase 

# Create your tests here.
class SimpleTests(SimpleTestCase):
    # Test home page loads
    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    # Test about page loads
    def test_about_page_status_code(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)