from django.shortcuts import render

# Using built-in TemplateView to display our templates.
from django.views.generic import TemplateView

# Create your views here.

"""
View that displays our homepage.
Inheriting from TemplateView and overwriting parameters.
View capitalized since it's a Python class.
"""
class HomePageView(TemplateView):
    template_name = 'home.html'

"""
View that displays the about page.
Uses TemplateView again.
"""
class AboutPageView(TemplateView):
    template_name = 'about.html'