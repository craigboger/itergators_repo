# pages/tests.py

from django.contrib.auth import get_user_model
from django.test import TestCase, SimpleTestCase
from django.urls import reverse

# Create your tests here.

# Tests for home page
class HomePageTests(SimpleTestCase):
    # Test home page loads successfully
    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # Test home page is using the correct url
    def test_view_url_by_name(self):
        # Get response based on querying for the home page url
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    # Test our home page is using the correct template
    def test_view_ises_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

# Tests for the user sign up page
class SignupPageTests(TestCase):
    # User credentials for test user database
    username = 'newuser'
    email = 'newuser@email.com'

    # Test that sign up page successfully loads
    def test_signup_page_status_code(self):
        response = self.client.get('/users/signup')
        print(reverse('signup'))
        self.assertEqual(response.status_code, 200)
    
    # Test correct URL is being mapped
    def test_view_url_by_name(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)

    # Test that correct template is being used for signup
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    # Test sign up form is functioning
    def test_signup_form(self):
        # Create new user in test database
        new_user = get_user_model().objects.create_user(
            self.username, self.email
        )
        # Verify user was added
        self.assertEqual(get_user_model().objects.all().count(), 1)
        # Make sure username and email are correctly stored in the user database
        self.assertEqual(get_user_model().objects.all()[0].username, self.username)
        self.assertEqual(get_user_model().objects.all()[0].email, self.email)
