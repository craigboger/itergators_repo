# pages/views.py
from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.

# Create view for homepage
class HomePageView(TemplateView):
    template_name = 'home.html'