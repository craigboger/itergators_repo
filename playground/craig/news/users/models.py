# users/models.py
from django.db import models
# Import abstract user we will extend to make our custom user(s)
from django.contrib.auth.models import AbstractUser

# Create your models here.

# Create a custom user with age field.
class CustomUser(AbstractUser):
    # Allow database to store "null" age for no value.
    # blank is validation related.  blank is allowed as an entry.
    age = models.PositiveIntegerField(null=True, blank=True)