from django.shortcuts import render

from django.urls import reverse_lazy
from django.views.generic import CreateView

# Import forms for views
from .forms import CustomUserCreationForm

# Create your views here.

# View to sign up a new user
class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'