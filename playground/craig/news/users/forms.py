# users/forms.py

# extend the default user creation and user change forms in django.
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

# Import the user model to use with the form.
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        #fields = UserCreationForm.Meta.fields + ('age',)
        fields = ('username', 'email', 'age',) #Password auto added since required

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        #fields = UserChangeForm.Meta.fields
        fields = ('username', 'email', 'age',) #Password auto added since required