from django.shortcuts import render
from .models import Post # Import Post model to display in view.
from django.views.generic import ListView  # Use list view generic to display database.

# Create your views here.
class HomePageView(ListView):
    model = Post
    template_name = 'home.html'
    context_object_name = 'all_posts_list'