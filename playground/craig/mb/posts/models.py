from django.db import models

# Create your models here.

# Create model as Python class.
class Post(models.Model):
    text = models.TextField()

    # Function to display the first 50 characters of the post.
    # __str__ is a function that returns a value from the
    # class object when expecting type string.
    def __str__(self):
        return self.text[:50]