# parent_portal/urls.py

from django.urls import path
from .views import (
    ParentPortalView,
)

urlpatterns = [
    path('', ParentPortalView, name='parent_portal'),
]