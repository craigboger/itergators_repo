# parent_portal/views.py

# IMPORTS
# ------------------------------------------------------------------------------
# For URL coordination and redirect
from django.shortcuts import render, redirect
from django.http import HttpResponse

# For message responses
from django.contrib import messages

# For Authorization
from django.contrib.auth.decorators import login_required

# Import models to use
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
    Parent
)

# Import decorators for authorization and checks
from accounts.decorators import (
    unauthenticated_user,
    student_required,
    teacher_required,
    parent_required,
    student_or_teacher_required
)

'''
IMPORTANT REFERENCES AND LINKS:

Django QuerySet API Reference
https://docs.djangoproject.com/en/3.1/ref/models/querysets/
https://www.youtube.com/watch?v=PD3YnPSHC-c&list=PL-51WBLyFTg2vW-_6XBoUpE7vpmoR3ztO&index=7

'''

# VIEWS
# ------------------------------------------------------------------------------
@login_required(login_url='login')  # If user not logged in, go to login page
@parent_required
def ParentPortalView(request):
    #TODO: Remove later
    # For giggles, querying all users in the database
    User = get_user_model()
    all_users = User.objects.all()
    all_students = Student.objects.all()
    all_parents = Parent.objects.all()
    all_teachers = Teacher.objects.all()
    context = {
        'all_users':all_users,
        'all_students':all_students,
        'all_parents':all_parents,
        'all_teachers':all_teachers,
        }
    return render(request, 'parent_portal/portal.html', context)
