# teacher_portal/forms.py

# extend the default user creation and user change forms in django.
from django import forms

# Make form transactions atomic to the database
from django.db import transaction

# Import models for use with the forms
from accounts.models import CustomUser, Student, Teacher, Parent
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# To make forms using modal.  Probably not going to be needed on teacher portal.
from bootstrap_modal_forms.forms import BSModalForm
from bootstrap_modal_forms.forms import BSModalModelForm

# Default django class to make normal forms from models
from django.forms import ModelForm
from django import forms

# FORMS
# ------------------------------------------------------------------------------

class UpdateClassWeekContentsForm(ModelForm):
    """
    Form to update the weekly class content on the teacher portal
    """
    class Meta:
        model = WeeklyClassroomContent
        fields = ('week_date_header', 'whiteboard_link')


class UpdateClassContentsForm(ModelForm):
    """
    Form to update classroom content not related to weeks on the
    teacher admin page.
    """
    # def __init__(self, *args, **kwargs):
    #     super(UpdateClassContentsForm, self).__init__(*args, **kwargs)
    #     # Set the choices for current week the form can use.
    #     curr_classroom = self.instance
    #     week_classroom_contents = curr_classroom.weeklyclassroomcontent_set.all().order_by('week_number')
    #     weeks_for_class = week_classroom_contents.order_by('week_number').values('week_number').distinct()
    #     week_list = []
    #     for week in weeks_for_class:
    #         week_list.append(('1', "Week " + str(week.get('week_number'))))
    #     print("-----------------Current Classroom Week Number")
    #     print(week_list)
    #     self.fields['current_week'].choices = forms.ChoiceField(choices=week_list)

    class Meta:
        model = Classroom
        # fields = ('class_zoom_link', 'teacher_zoom_link', 'tutor_zoom_link', 'class_available', 'teacher_available', 'tutor_available', 'current_week')
        fields = ('class_zoom_link', 'teacher_zoom_link', 'tutor_zoom_link', 'class_available', 'teacher_available', 'tutor_available')

class CreateNewWeekOfClassesAndSubjectsForm(BSModalForm):
    """
    For for creating new week of classroom and related subject pages with 
    standard modal input form.
    """
    #class_week_number = forms.IntegerField(min_value=1, max_value=36, required=True)
    class_week_number = forms.ChoiceField(choices=((1, "Week 1"),(2, "Week 2")))
    class_header = forms.CharField(required=True, max_length=300)
    whiteboard_link = forms.URLField(required=True)

    class Meta:
        fields = ('class_week_number', 'class_header', 'whiteboard_link')
    
    def __init__(self, *args, **kwargs):
        # Get keyword argument of queryset for curr_classroom
        curr_classroom = kwargs.pop('current_classroom')
        super(CreateNewWeekOfClassesAndSubjectsForm, self).__init__(*args, **kwargs)
        # Default set of choices for week number
        default_choices = []
        for i in range(1,19):
            default_choices.append((i, "Week " + str(i)))
        # Get all weeks associated with classroom
        week_classroom_contents = curr_classroom.weeklyclassroomcontent_set.all().order_by('week_number')
        # If no weeks associated, use default choices
        final_week_list = []
        if week_classroom_contents:
            weeks_for_class = week_classroom_contents.order_by('week_number').values('week_number').distinct()
            week_list = []
            for week in weeks_for_class:
                week_list.append((int(week.get('week_number')), "Week " + str(week.get('week_number'))))
            for week in default_choices:
                if week not in week_list:
                    final_week_list.append(week)
        else:
            final_week_list = default_choices
        # print("-----------------Current Classroom Week Number")
        # print(week_list)
        self.fields['class_week_number'].choices = final_week_list



class UpdateSubjectWeekContentsForm(ModelForm):
    """
    Form to update content of the subject page based on week.
    """
    class Meta:
        model = WeeklySubjectContent
        fields = ('week_date_header', 'whiteboard_link')


class CreateNewAssignmentForm(BSModalModelForm):
    """
    Form to create new assignment in modal form
    """
    class Meta:
        model = Assignment
        #fields ='__all__'
        fields = ('title', 'description', 'assignment_file')

    # Get user and source assignment from context in view
    def save(self, user=None, source_weekly_subject_id=None):
        # print("------------ENTER FORM SAVE")
        new_assignment = super(CreateNewAssignmentForm, self).save(commit=False)
        # Find the weekly subject content to link assignment to
        curr_weekly_subject_content = WeeklySubjectContent.objects.get(id=source_weekly_subject_id)
        if source_weekly_subject_id:
            new_assignment.source_weekly_subject_content = curr_weekly_subject_content
            print(new_assignment.source_weekly_subject_content)
        new_assignment.save()
        # print("----------EXIT FORM SAVE")
        return new_assignment


class UpdateAssignmentForm(BSModalModelForm):
    """
    Form to create new assignment in modal form
    """
    class Meta:
        model = Assignment
        #fields ='__all__'
        fields = ('title', 'description', 'assignment_file')


class GradeAssignmentForm(BSModalModelForm):
    """
    Form to create new assignment in modal form
    """
    class Meta:
        model = SubmittedAssignment
        #fields ='__all__'
        fields = ('grade', 'feedback', 'is_graded')

class TeacherUpdateSubmittedAssignmentForm(BSModalModelForm):
    '''
    Form to update a student's submitted assignment with grade
    and feedback.
    '''
    class Meta:
        model = SubmittedAssignment
        #fields ='__all__'
        fields = ('feedback', 'grade', 'is_graded')


class StudentGuardianAndEmergencyContactForm(BSModalModelForm):
    '''
    Form to be used with showing student contact info on modal.
    '''
    class Meta:
        model = Student
        fields = (
            'guardian1_firstname', 
            'guardian1_lastname',
            'guardian1_email',
            'guardian1_phone',
            'guardian2_firstname', 
            'guardian2_lastname',
            'guardian2_email',
            'guardian2_phone',
            'emergency_contact_firstname',
            'emergency_contact_lastname',
            'emergency_contact_email',
            'emergency_contact_phone',
        )
