# teacher_portal/views.py

# IMPORTS
# ------------------------------------------------------------------------------
# For URL coordination and redirect
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse, reverse_lazy
from django import http

# For message responses
from django.contrib import messages

# For Authorization
from django.contrib.auth.decorators import login_required

# Import decorators for authorization and checks
from accounts.decorators import (
    unauthenticated_user,
    student_required,
    teacher_required,
    parent_required,
    student_or_teacher_required
)

# Import models to use
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
    Parent
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Class based views to use
from bootstrap_modal_forms.generic import (
    BSModalFormView,
    BSModalCreateView,
    BSModalUpdateView,
    BSModalDeleteView,
    BSModalReadView
)

# Forms to use
from teacher_portal.forms import (
    UpdateClassWeekContentsForm,
    UpdateClassContentsForm,
    CreateNewWeekOfClassesAndSubjectsForm,
    UpdateSubjectWeekContentsForm,
    CreateNewAssignmentForm,
    UpdateAssignmentForm,
    GradeAssignmentForm,
    TeacherUpdateSubmittedAssignmentForm,
    StudentGuardianAndEmergencyContactForm
)

# For aggregations and calculations
from django.db.models import Avg

# VIEWS
# ------------------------------------------------------------------------------
@login_required(login_url='login')  # If user not logged in, go to login page
@teacher_required
def TeacherPortalView(request, week_wanted=None):
    # local vars
    curr_user = None
    curr_teacher = None
    curr_classroom = None
    # Get the teacher accessing the page and classroom the teacher is associated with.
    curr_user = request.user
    curr_teacher = Teacher.objects.get(user__id=curr_user.id)
    curr_classroom = Classroom.objects.get(teacher__user__id=curr_teacher.user.id)
    # Get the set of students that attend the classroom
    attending_students = Student.objects.filter(stud_class__id=curr_classroom.id)
    # Get current week from database
    curr_database_week = curr_classroom.current_week
    # print("-------Current Database Week")
    # print(curr_database_week)

    # Use POST data to determine which submit button was pressed and perform appropriate actions
    if request.method != 'POST':
        if week_wanted == None:
            # Default to week setting in the database
            week_wanted = curr_classroom.current_week
            # Redirect so URL reflects current week
            return redirect(reverse(TeacherPortalView, args=(week_wanted,)))
        else:
            # Default to week setting in the database
            week_wanted = week_wanted

    if request.method == 'POST':
        # If selecting another week to display on the page
        if 'week_wanted_submit' in request.POST:
            # Get the current week, redirect if different week selected
            # Can be week from url, week from dropdown, or default week from teacher admin
            # print("-------Week in POST")
            # print(request.POST.get('week_wanted'))
            # print(curr_teacher)
            week_wanted = request.POST.get('week_wanted')
            return redirect(reverse(TeacherPortalView, args=(week_wanted,)))
        # If updating the weekly class contents for the selected week.
        if 'week_class_contents_update' in request.POST:
            current_week = curr_classroom.weeklyclassroomcontent_set.get(week_number=week_wanted)
            form = UpdateClassWeekContentsForm(request.POST, instance=current_week)
            if form.is_valid():
                form.save()
                return redirect(reverse(TeacherPortalView, args=(week_wanted,)))
        # If updating the class contents not related to a specific week
        if 'class_contents_update' in request.POST:
            form = UpdateClassContentsForm(request.POST, instance=curr_classroom)
            if form.is_valid():
                form.save()
                return redirect(reverse(TeacherPortalView, args=(week_wanted,)))
        # If trying to set a new current week for the classroom
        if 'set_current_week' in request.POST:
            curr_classroom.current_week = request.POST.get('curr_week_wanted')
            curr_classroom.save()
            return redirect(reverse(TeacherPortalView, args=()))
        if 'create_new_week' in request.POST:
            form = CreateNewWeekOfClassesAndSubjectsForm(request.POST, current_classroom=curr_classroom)
            if form.is_valid():
                # Grab needed data from the form
                # week_to_add = form.class_week_number
                # header_to_add = form.class_header
                # whiteboard_link_to_add = form.whiteboard_link
                week_to_add = request.POST.get('class_week_number')
                header_to_add = request.POST.get('class_header')
                whiteboard_link_to_add = request.POST.get('whiteboard_link')
                # Get either current classroom week contents and update it or create new week of classroom content
                
                # print("-------- Adding week to class")
                # print("-------- curr_classroom id")
                # print(curr_classroom.id)
                # print(week_to_add)
                # print(header_to_add)
                # print(whiteboard_link_to_add)
                generated_classroom, classroom_created = WeeklyClassroomContent.objects.get_or_create(
                    source_class=curr_classroom,
                    source_class__id=curr_classroom.id,
                    week_number=week_to_add
                )
                # print("-------- Generated classroom object")
                # print(generated_classroom)
                generated_classroom.week_number = week_to_add
                generated_classroom.source_class = Classroom.objects.get(id=curr_classroom.id)
                generated_classroom.week_date_header = header_to_add
                generated_classroom.whiteboard_link = whiteboard_link_to_add
                generated_classroom.save()
                # Get subjects associated with the classroom
                associated_subjects = curr_classroom.subjectpage_set.all().order_by('name')
                # For each associated subject, create a new week associated with that subject.
                # Go ahead and auto set field with content
                for an_subject in associated_subjects:
                    generated_weekly_subject, subject_week_created = WeeklySubjectContent.objects.get_or_create(
                        source_subject=an_subject,
                        source_subject__id=an_subject.id,
                        week_number=week_to_add
                    )
                    generated_weekly_subject.week_number = week_to_add
                    generated_weekly_subject.source_subject = SubjectPage.objects.get(id=an_subject.id)
                    generated_weekly_subject.week_date_header = header_to_add
                    generated_weekly_subject.whiteboard_link = whiteboard_link_to_add
                    generated_weekly_subject.save()

                return redirect(reverse(TeacherPortalView, args=(week_to_add,)))
        # TODO: Insert more submit buttons here
    
    # Set current week for comparison in selection
    current_week = curr_classroom.weeklyclassroomcontent_set.get(week_number=week_wanted)
    # Get set of contents for weeks that exist for the classroom
    week_classroom_contents = curr_classroom.weeklyclassroomcontent_set.all().order_by('week_number')
    
    # Get the subject list to provide for footer subject page buttons
    curr_class_subjects = curr_classroom.subjectpage_set.all().order_by('name')

    # Grab the necessary forms for the classroom admin page.
    classroom_weekly_contents_update_form = UpdateClassWeekContentsForm(instance=current_week)
    classroom_contents_update_form = UpdateClassContentsForm(instance=curr_classroom)
    create_class_week_form = CreateNewWeekOfClassesAndSubjectsForm(current_classroom=curr_classroom)

    # print("----------- Week Choices")
    # print(classroom_contents_update_form)


    # print("-------Current Teacher")
    # print(curr_teacher)
    # print("-------Current Classroom")
    # print(curr_classroom)
    # print("-------Student List")
    # print(attending_students)
    # print("-------Current Week")
    # print(current_week)
    # print("-------Classroom Weeks")
    # print(week_classroom_contents)
    # Get the classroom object the t
    context = {
        # Vars
        'attending_students':attending_students,
        'current_week':current_week,
        'week_classroom_contents':week_classroom_contents,
        'curr_database_week':curr_database_week,
        'class_subjects':curr_class_subjects,
        'curr_classroom':curr_classroom,

        # Forms
        'classroom_weekly_contents_update_form':classroom_weekly_contents_update_form,
        'classroom_contents_update_form': classroom_contents_update_form,
        'create_class_week_form':create_class_week_form
    }
    return render(request, 'teacher_portal/portal.html', context)


@login_required(login_url='login')  # If user not logged in, go to login page
@teacher_required
def SubjectPageAdminView(request, subject_id, week_wanted=None):
    # local vars
    curr_user = None
    curr_teacher = None
    curr_classroom = None
    curr_subject = None
    # Get info about the teacher, subject, and classroom
    curr_user = request.user
    curr_teacher = Teacher.objects.get(user__id=curr_user.id)
    curr_subject = SubjectPage.objects.get(id=subject_id)
    curr_classroom = curr_subject.source_class
    week_subject_contents = curr_subject.weeklysubjectcontent_set.all().order_by('week_number')
    # Get the set of students that attend the classroom
    attending_students = Student.objects.filter(stud_class__id=curr_classroom.id)
        
    # Use POST data to determine which submit button was pressed and perform appropriate actions
    # Set week.  Default to setting in database if week not provided
    if request.method != 'POST':
        if week_wanted == None:
            # Default to week setting in the database
            week_wanted = curr_classroom.current_week
            # Redirect so URL reflects current week
            return redirect(reverse(SubjectPageAdminView, args=(curr_subject.id, week_wanted,)))
        else:
            # Default to week setting in the database
            week_wanted = week_wanted

    # Handle any POST requests (mostly submit buttons on forms)
    if request.method == 'POST':
        # If selecting different week for the page
        if 'week_wanted_submit' in request.POST:
            # Get the current week, redirect if different week selected
            # Can be week from url, week from dropdown, or default week from teacher admin
            week_wanted = request.POST.get('week_wanted')
            return redirect(reverse(SubjectPageAdminView, args=(curr_subject.id, week_wanted,)))
        # If updating the main contents of the subject page
        if 'week_subject_contents_update' in request.POST:
            current_week = curr_subject.weeklysubjectcontent_set.get(week_number=week_wanted)
            form = UpdateSubjectWeekContentsForm(request.POST, instance=current_week)
            if form.is_valid():
                form.save()
                return redirect(reverse(SubjectPageAdminView, args=(curr_subject.id, week_wanted,)))

    # Set current week for comparison in selection
    current_subject_week = curr_subject.weeklysubjectcontent_set.get(week_number=week_wanted)
    # Get assignments associated with the week and subject
    assignments_for_subject_week = Assignment.objects.filter(source_weekly_subject_content__id=current_subject_week.id, source_weekly_subject_content__week_number=week_wanted)
    # Get all submitted assignments that need grading.  To be used for grading queue.
    submitted_assignments_needing_grade = SubmittedAssignment.objects.filter(
        is_graded=False, 
        source_assignment__source_weekly_subject_content__source_subject__id=curr_subject.id
    )

    # Grab the necessary forms for the subject admin page.
    subject_weekly_contents_update_form = UpdateSubjectWeekContentsForm(instance=current_subject_week)
    
    # Setup and return page context
    context = {
        # Vars
        'curr_user':curr_user,
        'curr_teacher':curr_teacher,
        'curr_classroom':curr_classroom,
        'curr_subject':curr_subject,
        'attending_students':attending_students,
        'week_subject_contents':week_subject_contents,
        'current_subject_week':current_subject_week,
        'assignments_for_subject_week':assignments_for_subject_week,
        'submitted_assignments_needing_grade':submitted_assignments_needing_grade,
        
        # Forms
        'subject_weekly_contents_update_form':subject_weekly_contents_update_form
    }
    return render(request, 'teacher_portal/subject_portal.html', context)


# View to use in modal form for submitting a new assignment
class CreateNewAssignmentView(BSModalCreateView):
    # print("---- Entered view for create new assignment")
    template_name = 'teacher_portal/create_assignment.html'
    form_class = CreateNewAssignmentForm
    success_message = 'Success: Assignment Created.' 
    
    # Function to redirect back to the subject admin page after creating assignment.
    def get_success_url(self):
        # print("---- Entered Get Success URL for create new assignment")
        curr_subject_week_content_id = self.kwargs['subject_week_content_id']
        curr_subject_week_content = WeeklySubjectContent.objects.get(id=curr_subject_week_content_id)
        curr_subject = curr_subject_week_content.source_subject
        week_num_wanted = curr_subject_week_content.week_number
        return reverse(SubjectPageAdminView, args=(curr_subject.id, week_num_wanted))
        #return reverse_lazy('home')   

    def form_valid(self, form):
        if not self.request.is_ajax():
            curr_subject_week_content_id = self.kwargs['subject_week_content_id']
            # curr_subject_week_content = WeeklySubjectContent.objects.get(id=curr_subject_week_content_id)
            self.object = form.save(self.request.user, curr_subject_week_content_id)
        return http.HttpResponseRedirect(self.get_success_url())


# View to use in modal form for updating a submitted assignment
class TeacherUpdateAssignmentView(BSModalUpdateView):
    template_name = 'teacher_portal/update_assignment.html'
    form_class = UpdateAssignmentForm
    success_message = 'Success: Assignment Updated.'
    model = Assignment

    # Function to redirect to the custom assignment page url on successful 
    # submission of form.
    def get_success_url(self):
        # Get data from POST request
        curr_subject_week_content_id = self.kwargs['pk_subject_week_content']
        curr_subject_week_content = WeeklySubjectContent.objects.get(id=curr_subject_week_content_id)
        curr_subject = curr_subject_week_content.source_subject
        week_num_wanted = curr_subject_week_content.week_number
        return reverse(SubjectPageAdminView, args=(curr_subject.id, week_num_wanted))


class DeleteAssignmentView(BSModalDeleteView):
    model = Assignment
    template_name = 'teacher_portal/delete_assignment_confirm.html'
    success_message = 'Success: Assignment Deleted.'
    # success_url = reverse_lazy('teacher_portal')

    # Function to redirect to the subject admin page after successful deletion.
    def get_success_url(self):
        print("----------- Attempting to get success URL")
        # Get data from POST request
        curr_subject_week_content_id = self.kwargs['pk_subject_week_content']
        curr_subject_week_content = WeeklySubjectContent.objects.get(id=curr_subject_week_content_id)
        curr_subject = curr_subject_week_content.source_subject
        week_num_wanted = curr_subject_week_content.week_number
        return reverse(SubjectPageAdminView, args=(curr_subject.id, week_num_wanted))

# View to use in modal form for updating a submitted assignment
class TeacherGradeAssignmentView(BSModalUpdateView):
    template_name = 'teacher_portal/grade_submitted_assignment.html'
    form_class = GradeAssignmentForm
    success_message = 'Success: Assignment Graded.'
    model = SubmittedAssignment

    # Function to redirect to the subject admin page after successful grading
    def get_success_url(self):
        # Get data from POST request
        curr_submitted_assignment_id = self.kwargs['pk']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_subject = curr_submitted_assignment.source_assignment.source_weekly_subject_content.source_subject
        return reverse(SubjectPageAdminView, args=(curr_subject.id,))
    
    # Also update the graded status when grading the assignment
    def form_valid(self, form):
        redirect_url = super(TeacherGradeAssignmentView, self).form_valid(form)
        # Update graded status
        curr_submitted_assignment_id = self.kwargs['pk']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_submitted_assignment.is_graded = True
        curr_submitted_assignment.save()
        return redirect_url

    # Overload this function to get extra context info to template in
    # class based view.
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Grab submitted assignment so we can add submission download link to modal
        curr_submitted_assignment_id = self.kwargs['pk']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        # Add to context
        context['curr_submitted_assignment'] = curr_submitted_assignment
        context['curr_assignment'] = curr_submitted_assignment.source_assignment
        return context


@login_required(login_url='login')  # If user not logged in, go to login page
@teacher_required
def StudentProfileAndAssignmentsView(request, pk_subjectpage, pk_student_user):
    # Get the current user accessing the page
    curr_teacher_user = request.user
    curr_student = Student.objects.get(user__id=pk_student_user)
    # Get the current subject based on the primary key provided
    curr_subject = SubjectPage.objects.get(id=pk_subjectpage)
    curr_class = curr_subject.source_class
    # Get all assignments associated with the subject
    assignments_for_subject = Assignment.objects.filter(source_weekly_subject_content__source_subject__id=pk_subjectpage)
    # Get all assignments for the current subject that the user has submitted
    submitted_assignments = SubmittedAssignment.objects.filter(source_student__user__id=curr_student.user.id, source_assignment__source_weekly_subject_content__source_subject__id=curr_subject.id)
    # Get all assignments the student did not submit
    assigments_id_that_were_submitted = submitted_assignments.select_related('source_assignment').values('source_assignment_id')
    assignments_not_submitted = Assignment.objects.exclude(id__in=assigments_id_that_were_submitted)
    # Getting assignment objects of those assignments that were submitted
    assignments_submitted = Assignment.objects.filter(id__in=assigments_id_that_were_submitted)
    # Getting weekly subject content objects of those assignments submitted
    weekly_subject_ids_with_submitted_assignments = assignments_submitted.select_related('source_weekly_subject_content').values('source_weekly_subject_content_id')
    # Filter down to weekly subjects that have submitted assignments for the student.
    weekly_subject_content_of_submitted_assignments = WeeklySubjectContent.objects.filter(id__in=weekly_subject_ids_with_submitted_assignments)
    # Extract unique week numbers from set
    weeks_with_submitted_assignments = weekly_subject_content_of_submitted_assignments.order_by('week_number').values('week_number').distinct()
    # Get average grade for all submitted assignments
    student_average = submitted_assignments.aggregate(Avg('grade'))
    # Round if the average is available.
    if student_average['grade__avg']:
        student_average = round(student_average['grade__avg'], 2)
    else:
        student_average = student_average['grade__avg']

    context = {
        'curr_student':curr_student,
        'curr_subject':curr_subject,
        'curr_class':curr_class,
        'assignments_for_subject':assignments_for_subject,
        'submitted_assignments':submitted_assignments,
        'unsubmitted_assignments':assignments_not_submitted,
        'weeks_with_submitted_assignments':weeks_with_submitted_assignments,
        'student_average':student_average
    }
    return render(request, 'teacher_portal/student_profile_and_assignments.html', context)



class TeacherUpdateSubmittedAssignmentView(BSModalUpdateView):
    '''
    View to update a submitted assignment with grade and feedback.
    '''
    template_name = 'teacher_portal/teacher_update_assignment.html'
    form_class = TeacherUpdateSubmittedAssignmentForm
    success_message = 'Success: Assignment Updated.'
    model = SubmittedAssignment

    # Function to redirect to the custom assignment page url on successful 
    # submission of form.
    def get_success_url(self):
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_subject = curr_assignment.source_weekly_subject_content.source_subject
        curr_student_user = curr_submitted_assignment.source_student.user
        return reverse(StudentProfileAndAssignmentsView, args=(curr_subject.id, curr_student_user.id))
        #return reverse_lazy('home')   

    # Function to get the current assignment in the view.
    def get_curr_assignment(self):
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        return curr_assignment

    # Overload this function to get extra context info to template in
    # class based view.
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_user_id = self.kwargs['pk_user']
        curr_user = CustomUser.objects.get(id=curr_user_id)
        # Add to context
        context['curr_assignment'] = curr_assignment
        context['curr_user'] = curr_user
        context['curr_submitted_assignment'] = curr_submitted_assignment
        return context


class StudentGuardianAndEmergencyContactView(BSModalReadView):
    '''
    View to view student guardian and emergency contact info
    '''
    template_name = 'teacher_portal/student_contact_info.html'
    form_class = StudentGuardianAndEmergencyContactForm
    success_message = 'Success: Contact Info Viewed.'
    model = Student

    # Function to redirect to the custom assignment page url on successful 
    # submission of form.
    def get_success_url(self):
        curr_user_id = self.kwargs['pk']
        curr_student = Student.objects.get(id=curr_user_id)
        curr_class = Classroom.objects.get(id=curr_student.stud_class.id)
        return reverse(TeacherPortalView)

    # Overload this function to get extra context info to template in
    # class based view.
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Get current student selected
        curr_user_id = self.kwargs['pk']
        curr_student = Student.objects.get(user_id=curr_user_id)
        # Add to context
        context['curr_student'] = curr_student
        return context

# View to use in modal form for creating new week of class and subjects
# TODO: DELETE URL AND VIEW LATER IF NOT ACTUALLY USED
class CreateNewWeekOfClassesAndSubjectsView(BSModalFormView):
    template_name = 'teacher_portal/submit_new_week.html'
    form_class = CreateNewWeekOfClassesAndSubjectsForm
    success_message = 'Success: New week created.'  

    # Redirect to teacher admin portal after successful submission.
    def get_success_url(self):
        class_id = self.kwargs['pk_class']
        curr_class = Classroom.objects.get(id=class_id)
        return reverse(TeacherPortalView, args=())

    def form_valid(self, form):
        ''' This method is called when valid form data has been POSTed.'''
        # Create a form instance and populate it with data from the request (binding):
        #form = CreateNewWeekOfClassesAndSubjectsForm(request.POST)

        # Form should be already created and validated for us.

        # Get the class we are wanting to add a week to.
        curr_class_id = self.kwargs['pk_class']
        curr_classroom = Classroom.objects.get(id=curr_class_id)
        # Grab needed data from the form
        week_to_add = form.class_week_number
        header_to_add = form.class_header
        whiteboard_link_to_add = form.whiteboard_link
        # Get either current classroom week contents and update it or create new week of classroom content
        generated_classroom = WeeklyClassroomContent.objects.get_or_create(
            source_class__id=curr_classroom.id,
            week_number=week_to_add
        )
        generated_classroom.week_number = week_to_add
        generated_classroom.source_class = curr_classroom
        generated_classroom.week_date_header = header_to_add
        generated_classroom.whiteboard_link = whiteboard_link_to_add
        generated_classroom.save()
        # Get subjects associated with the classroom
        associated_subjects = curr_classroom.subjectpage_set.all().order_by('name')
        # For each associated subject, create a new week associated with that subject.
        # Go ahead and auto set field with content
        for an_subject in associated_subjects:
            generated_weekly_subject = WeeklySubjectContent.objects.get_or_create(
                source_subject__id=an_subject.id,
                week_number=week_to_add
            )
            generated_weekly_subject.week_number = week_to_add
            generated_weekly_subject.source_subject = an_subject
            generated_weekly_subject.week_date_header = header_to_add
            generated_weekly_subject.whiteboard_link = whiteboard_link_to_add
            generated_weekly_subject.save()

        # Return http response after completing form processing
        return http.HttpResponseRedirect(self.get_success_url())