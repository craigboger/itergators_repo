# teacher_portal/urls.py

from django.urls import path
from .views import (
    TeacherPortalView,
    CreateNewWeekOfClassesAndSubjectsView,
    SubjectPageAdminView,
    CreateNewAssignmentView,
    TeacherUpdateAssignmentView,
    DeleteAssignmentView,
    TeacherGradeAssignmentView,
    StudentProfileAndAssignmentsView,
    TeacherUpdateSubmittedAssignmentView,
    StudentGuardianAndEmergencyContactView
)

urlpatterns = [
    path('', TeacherPortalView, name='teacher_portal'),
    path('admin_class_wk_<str:week_wanted>/', TeacherPortalView, name='teacher_portal'),
    path('create_new_week/class_<str:pk_class>/', CreateNewWeekOfClassesAndSubjectsView.as_view(), name='create_new_class_week'),
    path('admin_subject_id_<str:subject_id>/', SubjectPageAdminView, name='subject_admin_page'),
    path('admin_subject_id_<str:subject_id>/wk_<str:week_wanted>/', SubjectPageAdminView, name='subject_admin_page'),
    path('create_assignment/subject_week_content_id_<str:subject_week_content_id>/', CreateNewAssignmentView.as_view(), name='create_assignment_modal'),
    path('update_assignment/subject_week_content_id_<str:pk_subject_week_content>/assignment_id_<pk>/', TeacherUpdateAssignmentView.as_view(), name='teacher_update_assignment_modal'),
    path('delete_assignment/subject_week_content_id_<str:pk_subject_week_content>/assignment_id_<pk>/', DeleteAssignmentView.as_view(), name='delete_assignment_modal'),
    path('grade_assignment/submitted_assignment_id_<pk>/', TeacherGradeAssignmentView.as_view(), name='grade_assignment_assignment_modal'),
    path('student_profile_and_assignments/subject_<str:pk_subjectpage>/student_<str:pk_student_user>', StudentProfileAndAssignmentsView, name='student_profile_and_assignments_view'),
    path('update_submitted_assignment/assign_<str:pk_submitted_assignment>/user_<str:pk_user>/<pk>/', TeacherUpdateSubmittedAssignmentView.as_view(), name='teacher_update_submitted_assignment'),
    path('view_student_contact_info/<pk>', StudentGuardianAndEmergencyContactView.as_view(), name='student_contact_info_modal'),
]

