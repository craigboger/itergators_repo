# classroom/urls.py

from django.urls import path
from .views import (
    ClassroomView,
    SubjectPageView,
    SubjectPageDynView,
    AssignmentsPageView,
    SubmitAssignmentView,
    StudentUpdateAssignmentView,
    NoAssociatedClassView
)

urlpatterns = [
    path('', ClassroomView, name='home'),
    path('class_wk_<str:week_wanted>/', ClassroomView, name='home'),
    path('subject_page/', SubjectPageView, name='subject_page'),
    path('subject_pages/class_<str:pk_subjectpage>/', SubjectPageDynView, name='dynamic_subject_page'),
    path('subject_pages/class_<str:pk_subjectpage>/week_<str:week_wanted>/', SubjectPageDynView, name='dynamic_subject_page'),
    #path('assignments_page/', AssignmentsPageView, name='assignments_page'),
    path('assignments_page/subject_<str:pk_subjectpage>/', AssignmentsPageView, name='assignments_page'),
    path('submit_assignment/assign_<str:pk_assignment>/user_<str:pk_user>/', SubmitAssignmentView.as_view(), name='submit_assignment'),
    path('update_assignment/assign_<str:pk_submitted_assignment>/user_<str:pk_user>/<pk>/', StudentUpdateAssignmentView.as_view(), name='student_update_submitted_assignment'),
    path('no_associated_class/', NoAssociatedClassView, name='no_associated_class'),
    
]