# classroom/forms.py

# extend the default user creation and user change forms in django.
from django import forms


# Make form transactions atomic to the database
from django.db import transaction

# Import models for use with the forms
from accounts.models import CustomUser, Student, Teacher, Parent
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

from bootstrap_modal_forms.forms import BSModalModelForm


# Form to submit assignment from assignments page
class StudentSubmitAssignmentForm(BSModalModelForm):
    class Meta:
        model = SubmittedAssignment
        #fields ='__all__'
        fields = ('completed_assignment',)

    # Get user and source assignment from context in view
    def save(self, user=None, curr_assignment=None):
        submitted_assignment = super(StudentSubmitAssignmentForm, self).save(commit=False)
        if user:
            submitted_assignment.source_student = Student.objects.get(user__id=user.id)
            print(submitted_assignment.source_student)
        if curr_assignment:
            submitted_assignment.source_assignment = curr_assignment
            print(submitted_assignment.source_assignment)
        submitted_assignment.save()
        return submitted_assignment

# Form to update assignment from assignments page by student and get
class StudentUpdateAssignmentForm(BSModalModelForm):
    class Meta:
        model = SubmittedAssignment
        #fields ='__all__'
        fields = ('completed_assignment', )
