# classroom/tests/view.py

'''
This is mainly for route testing to ensure a user sees the appropriate
pages while navigating the teacher admin portal or classroom.
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Model Imports
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Files
from pathlib import Path
from django.core.files import File
import os
import glob
import shutil

# TESTS
# ------------------------------------------------------------------------------

class BaseTestCase(TestCase):
    '''
    Test case that contains the data necessary for setting up a
    test environment with student, teacher, classroom, submitted assingment, etc
    '''

    @classmethod
    def setUpTestData(cls):
        # Student info to generate student user with
        cls.username = 'testStudent'
        cls.password1 = 'testPass123'
        cls.password2 = 'testPass123'
        cls.first_name = 'Student'
        cls.last_name = 'Person'
        cls.birth_date = '2012-11-29'
        cls.guardian_1_first_name = 'Guard1First'
        cls.guardian_1_last_name = 'Guard1Last'
        cls.guardian_1_email = 'guard1@mail.com'
        cls.guardian_1_phone = '+12125552368'
        cls.guardian_2_first_name = 'Guard2First'
        cls.guardian_2_last_name = 'Guard2Last'
        cls.guardian_2_email = 'guard2@mail.com'
        cls.guardian_2_phone = '+12125552368'
        # Teacher info to generate teacher with.
        cls.teacher_username = 'testTeacher'
        cls.teacher_password = 'testPass123'
        
        # Create teacher user
        cls.user_teacher = get_user_model().objects.create_user(
            username = 'testTeacher',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'TeacherFirst',
            last_name = 'TeacherLast',
            is_teacher = True
        )
        # Create teacher entry in the database
        cls.test_teacher = Teacher.objects.create(
            user=cls.user_teacher
        )
        cls.test_teacher.save()
        # Create classroom that teacher oversees and student can be a part of.
        cls.test_classroom = Classroom.objects.create(
            name='Test Classroom',
            current_week=1,
            teacher=cls.test_teacher,
            class_zoom_link='https://www.defzoom_class.com',
            teacher_zoom_link='https://www.defzoom_teacher.com',
            tutor_zoom_link='https://www.defzoom_tutor.com'
        )
        cls.test_classroom.save()
        # Create weekly content to attach to the new classroom
        cls.test_weekly_class_content_wk1 = WeeklyClassroomContent.objects.create(
            week_number=1,
            week_date_header='Welcome to week 1',
            whiteboard_link='https://www.google.com',
            source_class=cls.test_classroom
        )
        cls.test_weekly_class_content_wk1.save()
        cls.test_weekly_class_content_wk2 = WeeklyClassroomContent.objects.create(
            week_number=2,
            week_date_header='Welcome to week 2',
            whiteboard_link='https://www.google.com',
            source_class=cls.test_classroom
        )
        cls.test_weekly_class_content_wk2.save()
        # Create student user and add student to the class
        cls.user_student = get_user_model().objects.create_user(
            username = 'testStudent',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'StudentFirst',
            last_name = 'StudentLast',
            is_student = True
        )
        cls.user_student.save()
        # Create student entry in database
        test_student = Student.objects.create(
            user=cls.user_student,
            stud_class=cls.test_classroom
        )
        test_student.save()
        # Create subjects to use with the classroom for navigation
        subject_1 = SubjectPage.objects.create(
            name='Test Subject 1',
            source_class=cls.test_classroom
        )
        subject_2 = SubjectPage.objects.create(
            name='Test Subject 2',
            source_class=cls.test_classroom
        )
        # Open logo files to use with subject 1 and 2
        cls.subject1 = subject_1 # Don't want to use cls variable in creating other objects
        cls.subject2 = subject_2
        # Open files for logos to use with the subject pages
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        f2 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        cls.subject1.logo.save('sub1_logo.png', File(f1), save=True)
        cls.subject2.logo.save('sub2_logo.png', File(f2), save=True)
        f1.close()
        f2.close()
        # Create weekly subject content for the subject pages
        sub1_wk1 = WeeklySubjectContent.objects.create(
            week_number=1,
            week_date_header='Welcome to week 1 of Subject 1!',
            whiteboard_link='https://www.google.com',
            source_subject=subject_1
        )
        sub1_wk2 = WeeklySubjectContent.objects.create(
            week_number=2,
            week_date_header='Welcome to week 2 of Subject 1!',
            whiteboard_link='https://www.google.com',
            source_subject=subject_1
        )
        sub2_wk1 = WeeklySubjectContent.objects.create(
            week_number=1,
            week_date_header='Welcome to week 1 of Subject 2!',
            whiteboard_link='https://www.google.com',
            source_subject=subject_2
        )
        sub2_wk2 = WeeklySubjectContent.objects.create(
            week_number=2,
            week_date_header='Welcome to week 2 of Subject 2!',
            whiteboard_link='https://www.google.com',
            source_subject=subject_2
        )
        cls.sub1_wk1 = sub1_wk1
        cls.sub1_wk2 = sub1_wk2
        cls.sub2_wk1 = sub2_wk1
        cls.sub2_wk2 = sub2_wk2
        
        # Create assignments for the week of each subject
        sub1_wk1_assn1 = Assignment.objects.create(
            title='Assignment 1 Subject 1 Week 1',
            description='An assignment(1) for subject 1, week 1.',
            source_weekly_subject_content=sub1_wk1
        )
        sub1_wk1_assn2 = Assignment.objects.create(
            title='Assignment 2 Subject 1 Week 1',
            description='An assignment(2) for subject 1, week 1.',
            source_weekly_subject_content=sub1_wk1
        )
        sub1_wk1_assn3 = Assignment.objects.create(
            title='Assignment 3 Subject 1 Week 1',
            description='An assignment(3) for subject 1, week 1.',
            source_weekly_subject_content=sub1_wk1
        )
        sub2_wk1_assn1 = Assignment.objects.create(
            title='Assignment 1 Subject 2 Week 1',
            description='An assignment(1) for subject 1, week 2.',
            source_weekly_subject_content=sub2_wk1
        )
        sub2_wk1_assn2 = Assignment.objects.create(
            title='Assignment 2 Subject 2 Week 1',
            description='An assignment(2) for subject 1, week 2.',
            source_weekly_subject_content=sub2_wk1
        )
        cls.sub1_wk1_assn1 = sub1_wk1_assn1
        cls.sub1_wk1_assn2 = sub1_wk1_assn2
        cls.sub2_wk1_assn1 = sub2_wk1_assn1
        cls.sub2_wk1_assn2 = sub2_wk1_assn2
        # Submit assignment 1 for each subject with file
        # Go ahead and grade for testing report card page
        sub1_wk1_assn1_submitted = SubmittedAssignment.objects.create(
            source_assignment=sub1_wk1_assn1,
            source_student=test_student,
            is_graded=True,
            grade=80
        )
        sub1_wk1_assn3_submitted = SubmittedAssignment.objects.create(
            source_assignment=sub1_wk1_assn3,
            source_student=test_student,
        )
        sub2_wk1_assn1_submitted = SubmittedAssignment.objects.create(
            source_assignment=sub2_wk1_assn1,
            source_student=test_student,
            is_graded=True,
            grade=85
        )
        # Open files for assignment to submit it
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        f2 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        sub1_wk1_assn1_submitted.completed_assignment.save('sub1_logo.png', File(f1), save=True)
        sub2_wk1_assn1_submitted.completed_assignment.save('sub2_logo.png', File(f2), save=True)
        f1.close()
        f2.close()

    def setUp(self):
        self.user_teacher = get_user_model().objects.filter(is_teacher=True).first()
        self.test_teacher = Teacher.objects.all().first()
        self.test_classroom = Classroom.objects.all().first()
        self.test_weekly_class_content_wk1 = WeeklyClassroomContent.objects.filter(week_number=1).first()
        self.test_weekly_class_content_wk2 = WeeklyClassroomContent.objects.filter(week_number=2).first()
        self.user_student = get_user_model().objects.filter(is_student=True).first()
        self.test_student = Student.objects.all().first()
        self.subject_1 = SubjectPage.objects.filter(name='Test Subject 1').first()
        self.subject_2 = SubjectPage.objects.filter(name='Test Subject 2').first()
        self.sub1_wk1 = WeeklySubjectContent.objects.filter(source_subject=self.subject_1, week_number=1).first()
        self.sub1_wk2 = WeeklySubjectContent.objects.filter(source_subject=self.subject_1, week_number=2).first()
        self.sub2_wk1 = WeeklySubjectContent.objects.filter(source_subject=self.subject_2, week_number=1).first()
        self.sub2_wk2 = WeeklySubjectContent.objects.filter(source_subject=self.subject_2, week_number=2).first()
        self.sub1_wk1_assn1 = Assignment.objects.filter(
            source_weekly_subject_content=self.sub1_wk1,
            title='Assignment 1 Subject 1 Week 1'
        ).first()
        self.sub1_wk1_assn2 = Assignment.objects.filter(
            source_weekly_subject_content=self.sub1_wk1,
            title='Assignment 2 Subject 1 Week 1'
        ).first()
        self.sub1_wk1_assn3 = Assignment.objects.filter(
            source_weekly_subject_content=self.sub1_wk1,
            title='Assignment 3 Subject 1 Week 1'
        ).first()
        self.sub2_wk1_assn1 = Assignment.objects.filter(
            source_weekly_subject_content=self.sub2_wk1,
            title='Assignment 1 Subject 2 Week 1'
        ).first()
        self.sub2_wk1_assn2 = Assignment.objects.filter(
            source_weekly_subject_content=self.sub2_wk1,
            title='Assignment 2 Subject 2 Week 1'
        ).first()
        # Login credentials to perform successful login
        self.student_login_credentials = {
            'username': 'testStudent',
            'password': 'testPass123'
        }
        self.teacher_login_credentials = {
            'username': 'testTeacher',
            'password': 'testPass123'
        }
        # Get submitted assignments
        self.sub1_wk1_assn1_submitted = SubmittedAssignment.objects.filter(source_assignment__id=self.sub1_wk1_assn1.id).first()
        self.sub2_wk1_assn1_submitted = SubmittedAssignment.objects.filter(source_assignment__id=self.sub2_wk1_assn1.id).first()
        self.sub1_wk1_assn3_submitted = SubmittedAssignment.objects.filter(source_assignment__id=self.sub1_wk1_assn3.id).first()

    def tearDown(self):
        # Find files and folders for the subject logos to remove
        test_dir = Path(__file__).parent.absolute()
        proj_dir = Path(__file__).parents[2].absolute()
        subject_static_dir = Path(proj_dir, 'static', 'uploaded_files', 'subject_page_content')
        search_file = str(subject_static_dir) + '/Test_*/*.png'
        search_folder = str(subject_static_dir) + '/Test_*'
        # Remove files and folders for the subject logos
        files = glob.glob(search_file, recursive=True)
        for f in files:
            try:
                os.remove(f)
            except OSError as e:
                print("Error: %s : %s" % (f, e.strerror))
        folders = glob.glob(search_folder, recursive=True)
        for f in folders:
            try:
                os.rmdir(f)
            except OSError as e:
                print("Error: %s : %s" % (f, e.strerror))
        # Find files and folders for the submitted assignments to remove
        submitted_assn_dir = Path(proj_dir, 'static', 'uploaded_files', 'submitted_assignments')
        search_folder = str(submitted_assn_dir) + '/Test_Classroom'

        try:
            shutil.rmtree(search_folder)
        except OSError as e:
            pass
            # Removing error since directory is in fact being removed
            # print("Error: %s : %s" % (search_folder, e.strerror))



class ClassroomPageTest(BaseTestCase):
    '''
    Test to make sure classroom page loads and is at the correct location.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login student and make sure student successfully logged in.
        user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('', follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('home'), follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('home', args=('1',)))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'classroom/classroom.html')

        
class SubjectPageTest(BaseTestCase):
    '''
    Test to make sure subject page loads and is at the correct location.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login student and make sure student successfully logged in.
        user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_view_url_exists_at_proper_location(self):
        link = '/subject_pages/class_' + str(self.subject_1.id) + '/week_1/'
        resp = self.client.get(link)
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('dynamic_subject_page', args=(self.subject_1.id, 1)))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('dynamic_subject_page', args=(self.subject_1.id, 1)))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'classroom/subject_page.html')


class ReportCardPageTest(BaseTestCase):
    '''
    Test to make sure subject page loads and is at the correct location.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login student and make sure student successfully logged in.
        user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_view_url_exists_at_proper_location(self):
        link = '/assignments_page/subject_' + str(self.subject_1.id) + '/'
        resp = self.client.get(link)
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('assignments_page', args=(self.subject_1.id,)))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('assignments_page', args=(self.subject_1.id,)))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'classroom/assignments_page.html')


class StudentClassNavigationTest(BaseTestCase):
    '''
    Class to perform tests that emulate student navigating through a
    generated classroom and subjects.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login student and make sure student successfully logged in.
        user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_student_classroom_and_subject_setup(self):
        '''
        Test to see if classroom is setup correctly for student including:
        1) Seeing if subject buttons correctly generated
        2) If links in those subject buttons go to the correct subject pages
        '''
        # Navigate to the classroom page after login
        response = self.client.get(reverse('home', args=(1,)))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm button links to subject pages exist on the page
        subject_1_button_link = f'/subject_pages/class_{self.subject_1.id}/'
        subject_2_button_link = f'/subject_pages/class_{self.subject_2.id}/'
        self.assertContains(response, subject_1_button_link, html=False)
        self.assertContains(response, subject_2_button_link, html=False)
        # Confirm that pressing the subject 1 button goes to the appropriate subject page
        # Buttons are simple link, so just use get request.
        sub1_response = self.client.get(reverse('dynamic_subject_page', args=(self.subject_1.id,)))
        self.assertEqual(sub1_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(sub1_response, 'classroom/subject_page.html') # Make sure correct template was used
        self.assertContains(sub1_response, str(self.sub1_wk1.week_date_header), html=False) # Make sure appropriate week is showing
        
    def test_student_report_card_setup(self):
        '''
        Test to see if report card has the below setup:
        1) Seeing submitted assignment on page
        2) If grade average is present on page
        '''
        # Navigate to the report card page after login
        response = self.client.get(reverse('assignments_page', args=(self.subject_1.id,)), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm submitted assignment on the page
        submitted_assignment_title = str(self.sub1_wk1_assn1_submitted.source_assignment.title)
        grade_average = str(self.sub1_wk1_assn1_submitted.grade)
        self.assertContains(response, submitted_assignment_title, html=False)
        # Confirm grade average is showing on the page
        self.assertContains(response, grade_average, html=False)

    def test_student_change_class_week(self):
        '''
        Test to make sure changing week works on classroom works.
        '''
        # Simulate requesting week 2 from post request
        response = self.client.post(reverse('home'), {'week_wanted': '2',}, follow=True)
        # Test if redirect successful
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Test that week 2 is showing.
        self.assertContains(
            response, 
            self.test_weekly_class_content_wk2.week_date_header, 
            html=False
        )
    
    def test_student_change_subject_week(self):
        '''
        Test to make sure changing week works on subject.
        '''
        # Simulate post request to subject page requesting week 2
        response = self.client.post(reverse('dynamic_subject_page', args=(self.subject_1.id,)), {'week_wanted': '2',}, follow=True)
        # Test if redirect successful
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Test that week 2 is showing.
        self.assertContains(
            response,
            self.sub1_wk2.week_date_header,
            html=False
        )

    def test_assignments_present_in_subject(self):
        '''
        Test to make unsubmitted assignment on subject page.
        '''
        # Since assignment 1 submitted for week 1 in each subject, we are 
        # searching for assignment 2 on page
        sub1_response = self.client.get(reverse('dynamic_subject_page', args=(self.subject_1.id, 1)))
        # Check if assignment 2 (unsubmitted) is on subject 1 page
        self.assertContains(
            sub1_response,
            self.sub1_wk1_assn2.title,
            html=False
        )
        sub2_response = self.client.get(reverse('dynamic_subject_page', args=(self.subject_2.id, 1)))
        # Check if assignment 2 (unsubmitted) is on subject 1 page
        self.assertContains(
            sub2_response,
            self.sub2_wk1_assn2.title,
            html=False
        )


# class TeacherClassNavigationTest(BaseTestCase):
#     '''
#     Class to perform tests that emulate teacher navigating through a
#     generated teacher admin portal for classroom and subjects.
#     '''

#     def setUp(self):
#         # Call base setup and add custom setup here
#         super().setUp()
#         # Go ahead and login teacher and make sure teacher successfully logged in.
#         user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
#         self.assertTrue(user)

#     def tearDown(self):
#         super().tearDown()
#         pass

