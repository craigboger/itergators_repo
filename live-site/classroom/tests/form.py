# classroom/tests/form.py

'''
This is mainly for route testing submission forms
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Model Imports
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Files
from pathlib import Path
from django.core.files import File
import os
import glob
import shutil

# Import other Base Test cases to build off of
from .view import BaseTestCase  # Base test case for setting up new database with student, teacher, classroom, assignment, etc


class StudentAssignmentSubmitFormTest(BaseTestCase):
    '''
    Tests to make sure the assignment submission form is working.
    '''

    def setUp(self):
            # Call base setup and add custom setup here
            super().setUp()
            # Go ahead and login student and make sure student successfully logged in.
            user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
            self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass
            
    def test_assignment_submission(self):
        '''
        Test of submitting assignment on subject 1 page.
        Will be submitting assignment 2 since assignment 1 already
        submitted.
        '''
        # Making direct post request to form since we already know the 
        # assignment button and linking works on the subject page.
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        submit_response = self.client.post(
            reverse('submit_assignment', args=(self.sub1_wk1_assn2.id, self.user_student.id)),
            {'completed_assignment': f1},
            follow=True
        )
        f1.close()
        # Confirm submission in database with file
        submitted_assignment = SubmittedAssignment.objects.filter(
            source_assignment=self.sub1_wk1_assn2,
            source_student=self.user_student.id
        ).first()
        self.assertTrue(submitted_assignment)
        # Confirm the user was redirected to the subject page again.
        self.assertTemplateUsed(submit_response, 'classroom/subject_page.html')


class StudentAssignmentUpdateFormTest(BaseTestCase):
    '''
    Tests to make sure the submission update form for the report card page functions.
    '''

    def setUp(self):
            # Call base setup and add custom setup here
            super().setUp()
            # Go ahead and login student and make sure student successfully logged in.
            user = self.client.login(username=self.student_login_credentials['username'], password=self.student_login_credentials['password'])
            self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass
            
    def test_submitted_assignment_update_form(self):
        '''
        Test of submitting assignment on subject 1 page.
        Will be submitting assignment 2 since assignment 1 already
        submitted.
        '''
        # Making direct post request to form since we already know the 
        # assignment button and linking works on the subject page.
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','abacus.png'), "rb")
        update_response = self.client.post(
            reverse('student_update_submitted_assignment', args=(self.sub1_wk1_assn1_submitted.id, self.user_student.id, self.sub1_wk1_assn1_submitted.id)),
            {'completed_assignment': f1},
            follow=True
        )
        f1.close()
        # Confirm submission in database with file
        updated_assignment = SubmittedAssignment.objects.filter(
            source_assignment=self.sub1_wk1_assn1,
            source_student=self.user_student.id
        ).first()
        self.assertTrue(updated_assignment)
        # Confirm new file url
        self.assertTrue(
            'abacus' in str(updated_assignment.completed_assignment.url)
        )
        # Confirm the user was redirected to the report card page again.
        self.assertTemplateUsed(update_response, 'classroom/assignments_page.html')