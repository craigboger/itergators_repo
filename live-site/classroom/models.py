# classroom/models.py

"""
Notebook:

Dynamic file paths for file upload in Django:
https://stackoverflow.com/questions/5135556/dynamic-file-path-in-django

"""

# Model Imports
from django.db import models
from accounts.models import (
    Student,
    Teacher,
    Parent
)
# Other
import os

# MODELS
# ------------------------------------------------------------------------------

class Classroom(models.Model):
    """
    Model to hold a classroom object
    """
    # Classroom Attributes
    name = models.CharField(blank=False, null=False, max_length=500)
    class_zoom_link = models.URLField(blank=True, null=True, max_length=500)
    class_available = models.BooleanField(default=False)
    teacher_zoom_link = models.URLField(blank=True, null=True, max_length=500)
    teacher_available = models.BooleanField(default=False)
    tutor_zoom_link = models.URLField(blank=True, null=True, max_length=500)
    tutor_available = models.BooleanField(default=False)
    current_week = models.PositiveSmallIntegerField(null=False, blank=False, default=1)
    # Relationship to teacher that owns the classroom
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.name

class WeeklyClassroomContent(models.Model):
    """
    Model to hold the weekly classroom data.
    """
    # Weekly Class Content Attributes
    week_number = models.PositiveSmallIntegerField(blank=False, null=False)
    week_date_header = models.CharField(blank=False, null=False, max_length=300, default="Welcome to Class!")
    whiteboard_link = models.URLField(blank=True, null=True, max_length=500)
    # Relationship to classroom for that week.
    source_class = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    def __str__(self):
        return (
            self.source_class.name + 
            "_week_" +
            str(self.week_number)
        )
    # Make sure there is only 1 week for every class
    class Meta:
        unique_together = (("source_class", "week_number"),)

class SubjectPage(models.Model):
    """
    Model to hold a subject page.
    Subject page is associated with a classroom
    """
    def get_upload_path(instance, filename):
        """ For dynamic filepaths for saving submitted assignments """
        upload_path = os.path.join(
            "subject_page_content",
            "%s" % str(instance.source_class.name) + str(instance.name) + "_" + str(instance.id),
            filename
        )
        # Remove spaces from file path
        upload_path = upload_path.replace(" ", "_")
        return upload_path
    # Subject Page Attributes
    name = models.CharField(blank=False, null=True, max_length=500)
    logo = models.ImageField(null=True, blank=True, upload_to=get_upload_path)
    # Relationship to classroom
    source_class = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    def __str__(self):
        return (
            self.source_class.name + 
            " - " +
            str(self.name)
        )

class WeeklySubjectContent(models.Model):
    # Weekly Subject Page Content Attributes
    week_number = models.PositiveSmallIntegerField(blank=False, null=False)
    whiteboard_link = models.URLField(blank=True, null=True, max_length=500)
    week_date_header = models.CharField(blank=False, null=False, max_length=300, default="Welcome to Class!")
    # Relationship to Subject Page Content is used in.
    source_subject = models.ForeignKey(SubjectPage, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.week_number)
    # Make sure there is only 1 week for every class
    class Meta:
        unique_together = (("source_subject", "week_number"),)
    # Function to display string summary of model.
    def __str__(self):
        return (
            self.source_subject.source_class.name + 
            "_" +
            self.source_subject.name + 
            "_week_" +
            str(self.week_number)
        )
    # Below functions used to provide column data for admin panel.
    # Function to get the associated class name
    def class_name(self):
        return str(self.source_subject.source_class.name)
    # Function to get the associated subject name
    def subject_name(self):
        return str(self.source_subject.name)

class Assignment(models.Model):
    """
    Model to hold created assignment
    """
    def get_upload_path(instance, filename):
        """ For dynamic filepaths for saving submitted assignments """
        upload_path = os.path.join(
            "created_assignments",
            "%s" % str(instance.source_weekly_subject_content.source_subject.source_class.name),
            "%s" % str(instance.source_weekly_subject_content.source_subject.name),
            "week_%s" % str(instance.source_weekly_subject_content.week_number),
            filename
        )
        # Remove spaces from file path
        upload_path = upload_path.replace(" ", "_")
        return upload_path

    # Assignment Attributes
    title = models.CharField(null=True, max_length=254)
    description = models.CharField(null=True, max_length=1000)
    assignment_file = models.FileField(blank=True, null=True, upload_to=get_upload_path)
    weight = models.DecimalField(blank=True, null=True, max_digits=5, decimal_places=2)
    # Relationship to weekly subject content
    source_weekly_subject_content = models.ForeignKey(WeeklySubjectContent, on_delete=models.CASCADE, null=True)
    def __str__(self):
        return (
            self.source_weekly_subject_content.source_subject.source_class.name + 
            "_" + 
            self.source_weekly_subject_content.source_subject.name + 
            "_wk_" + 
            str(self.source_weekly_subject_content.week_number) + 
            "_" + 
            self.title
        )
    # Below functions used to provide column data for admin panel.
    # Function to get the associated class name
    def class_name(self):
        return str(self.source_weekly_subject_content.source_subject.source_class.name)
    # Function to get the associated subject name
    def subject_name(self):
        return str(self.source_weekly_subject_content.source_subject.name)
    # Function to get the week the assignment was assigned
    def assignment_week(self):
        return str(self.source_weekly_subject_content.week_number)

class SubmittedAssignment(models.Model):
    """
    Model to hold a completed assignment
    """
    def get_upload_path(instance, filename):
        """ For dynamic filepaths for saving submitted assignments """
        upload_path = os.path.join(
            "submitted_assignments",
            "%s" % str(instance.source_assignment.source_weekly_subject_content.source_subject.source_class.name),
            "%s" % str(instance.source_assignment.source_weekly_subject_content.source_subject.name),
            "week_%s" % str(instance.source_assignment.source_weekly_subject_content.week_number),
            "%s" % str(
                instance.source_student.user.last_name +
                "_" +
                instance.source_student.user.first_name
                ),
            filename
        )
        # upload_path = os.path.join("submitted_assignments", str(filename))
        # Remove spaces from file path
        upload_path = upload_path.replace(" ", "_")
        return upload_path
    # Relationship to source assignment (one-to-many, many submitted assignments for each created assignment)
    source_assignment = models.ForeignKey(Assignment, on_delete=models.SET_NULL, null=True)
    # Relationship to source student (one-to-many, many submitted assignments for each created assignment)
    source_student = models.ForeignKey(Student, on_delete=models.SET_NULL, null=True)
    # Submitted Assignment Attributes
    is_graded = models.BooleanField(default=False)              # Mark assignment as graded.
    grade = models.PositiveIntegerField(blank=True, null=True)  # Grade for the assignment.
    completed_assignment = models.FileField(blank=True, upload_to=get_upload_path)       # File containing completed work.
    feedback = models.CharField(blank=True, null=True, max_length=1000)     # Provide feedback on student work.
    
    def __str__(self):
        return (
            self.source_assignment.title +
            " - " +
            self.source_student.user.first_name +
            " " + 
            self.source_student.user.last_name
        )
    # Below functions used to provide column data for admin panel.
    # Function to get the associated class name
    def class_name(self):
        return str(self.source_assignment.source_weekly_subject_content.source_subject.source_class.name)
    # Function to get the associated subject name
    def subject_name(self):
        return str(self.source_assignment.source_weekly_subject_content.source_subject.name)
    # Function to get the associated assignment name
    def assignment_name(self):
        return str(self.source_assignment.title)
    # Function to get the week the assignment was assigned
    def assignment_week(self):
        return str(self.source_assignment.source_weekly_subject_content.week_number)
    # Function to get the student that turned in assignment
    def associated_student_last_name(self):
        return str(self.source_student.user.last_name)
    def associated_student_first_name(self):
        return str(self.source_student.user.first_name)