# accounts/decorators.py

# IMPORTS
# ------------------------------------------------------------------------------
from django.http import HttpResponse
from django.shortcuts import redirect

# Using Django built in decorators
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test

# IMPLEMENTATION OF DECORATORS
# ------------------------------------------------------------------------------

# Create decorators that use user flags
'''
Simple is better than complex article on user redirection and authorization
https://simpleisbetterthancomplex.com/tutorial/2018/01/18/how-to-implement-multiple-user-types-with-django.html
'''
def student_required(
    function=None, 
    redirect_field_name=REDIRECT_FIELD_NAME,
    login_url='login'):
    '''
    Decorator for views that checks that the logged in user is a student,
    redirects to login page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda user: user.is_active and user.is_student,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def teacher_required(
    function=None, 
    redirect_field_name=REDIRECT_FIELD_NAME,
    login_url='login'):
    '''
    Decorator for views that checks that the logged in user is a teacher,
    redirects to login page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda user: user.is_active and user.is_teacher,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def parent_required(
    function=None, 
    redirect_field_name=REDIRECT_FIELD_NAME,
    login_url='login'):
    '''
    Decorator for views that checks that the logged in user is a student,
    redirects to login page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda user: user.is_active and user.is_parent,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def student_or_teacher_required(
    function=None, 
    redirect_field_name=REDIRECT_FIELD_NAME,
    login_url='login'):
    '''
    Decorator for views that checks that the logged in user is a student or teacher,
    redirects to login page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda user: user.is_active and (user.is_student or user.is_teacher),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


# Below are older style decorators from russian guy tutorial

'''
A decorator is a function that takes in another function as a 
parameter.  Let's us add functionality before the function 
below the decorator is called.

It will physically take the function below it with its arguements
and pass it to the wrapper function in the decorator function.
'''

# Decorator to stop an authenticated user from going to login or registration page.
def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        # Run some conditionals and check before handing the 
        # function back.
        
        # If user is already logged in, redirect to home page.
        # Don't want already logged in user going to login page.
        if request.user.is_authenticated:
            return redirect('home')
        else:
            # If user is not already authenticated, proceed.
            return view_func(request, *args, **kwargs)

    return wrapper_func

# # Decorator to make sure a certain user type is allowed on a certain page.
# def allowed_users(allowed_roles=[]):
#     # Since we are passing arguments, we will put view_func
#     # in this function.
#     def decorator(view_func):
#         def wrapper_func(request, *args, **kwargs):
#             # Decide here to redirect user or return function.
#             group = None
#             # Check if the user is part of a group
#             if request.user.groups.exists():
#                 # Grab first group user belongs to in the list.
#                 group = request.user.groups.all()[0].name
#             # Check if the group is in the allowed roles.
#             if group in allowed_roles:
#                 # If user is in specified group list, proceed.
#                 return view_func(request, *args, **kwargs)
#             else:
#                 # Tell the user they are not authorized
#                 return HttpResponse('You are not authorized to view this page.')
#         return wrapper_func
#     return decorator

# # Decorator to check the user group.
# # Quick fix TODO: Remove
# # Redirect user to page they are authorized to access.
# def student_only(view_func):
#     def wrapper_func(request, *args, **kwargs):
#         group = None
        
#         if request.user.groups.exists():
#             group = request.user.groups.all()[0].name
        
#         if group == 'Parent':
#             return redirect('logout')
#         if group == 'Teacher':
#             return redirect('logout')
#         if group == 'Student':
#             return view_func(request, *args, **kwargs)
#         # If no group what so ever
#         else:
#             return redirect('logout')

#     return wrapper_func