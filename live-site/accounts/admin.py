# accounts/admin.py
'''
Stackoverflow link to creating admin page with custom inline fields
https://stackoverflow.com/questions/15012235/using-django-auth-useradmin-for-a-custom-user-model
https://stackoverflow.com/questions/15012235/using-django-auth-useradmin-for-a-custom-user-model
'''

from django.contrib import admin

# Import items to help create admin section for custom user model.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import (
    CustomUser,
    Student,
    Teacher,
    Parent,
)

# Define an inline admin descriptor for the Student model
# which acts a bit like a singleton
class StudentInline(admin.StackedInline):
    model = Student
    can_delete = False
    verbose_name_plural = 'student'

# Define an inline admin descriptor for the Teacher model
# which acts a bit like a singleton
class TeacherInline(admin.StackedInline):
    model = Teacher
    can_delete = False
    verbose_name_plural = 'teacher'

# Define an inline admin descriptor for the Parent model
# which acts a bit like a singleton
class ParentInline(admin.StackedInline):
    model = Parent
    can_delete = False
    verbose_name_plural = 'parent'

# Define a new user admin page.
class CustomUserAdmin(UserAdmin):
    # Setup user admin form for custom user type.
    model = CustomUser
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    # Designate additional inline models in the user admin
    inlines = (StudentInline, TeacherInline, ParentInline)
    # Designate columns in user list in admin console
    list_display = ['first_name', 'last_name', 'username', 'email', 'is_staff','is_student', 'is_teacher', 'is_parent']
    # Designate fields to filter by
    list_filter = ('is_student', 'is_teacher', 'is_parent')
    fieldsets = UserAdmin.fieldsets + (
        ('User Roles', {'fields':('is_student', 'is_teacher', 'is_parent')}),
    )

# Register custom admin form for the user.
admin.site.register(CustomUser, CustomUserAdmin)
