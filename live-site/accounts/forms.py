# accounts/forms.py

# extend the default user creation and user change forms in django.
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from phonenumber_field.formfields import PhoneNumberField

# Make form transactions atomic to the database
from django.db import transaction

# Import the user model to use with the form.
from .models import CustomUser, Student, Teacher, Parent

import datetime


# Replica of the Django built in user creation form.
class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser
        # https://docs.djangoproject.com/en/3.1/topics/auth/customizing/
        #fields = ('username', 'email', 'first_name', 'last_name', 'is_student', 'is_teacher', 'is_parent',) #Password auto added since required
        fields = UserCreationForm.Meta.fields + ('is_student', 'is_teacher', 'is_parent',)

class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = CustomUser
        #fields = ('username', 'email', 'first_name', 'last_name', 'is_student', 'is_teacher', 'is_parent') #Password auto added since required

# Forms for registering student
class StudentCreationForm(UserCreationForm):
    first_name = forms.CharField(required=True, max_length=100)
    last_name = forms.CharField(required=True, max_length=100)
    birth_date = forms.DateField(initial=datetime.date.today)
    # Get guardian contact info
    guardian_1_first_name = forms.CharField(required=True, max_length=100)
    guardian_1_last_name = forms.CharField(required=True, max_length=100)
    guardian_1_email = forms.EmailField(required=True)
    guardian_1_phone = PhoneNumberField(required=True)

    guardian_2_first_name = forms.CharField(required=False, max_length=100)
    guardian_2_last_name = forms.CharField(required=False, max_length=100)
    guardian_2_email = forms.EmailField(required=False)
    guardian_2_phone = PhoneNumberField(required=False)

    class Meta(UserCreationForm):
        model = CustomUser
        fields = (
            'username', 'first_name', 'last_name', 'birth_date',
            'guardian_1_first_name', 'guardian_1_last_name', 
            'guardian_1_email', 'guardian_1_phone',
            'guardian_2_first_name', 'guardian_2_last_name',
            'guardian_2_email', 'guardian_2_phone'
        ) #Password auto added since required

    @transaction.atomic
    def save(self):
        # Create user and set user specific attributes.
        user = super().save(commit=False) # Saving instance of our user.
        # Set commit to false so we can save the below info before committing to database.
        user.is_student = True
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.email = self.cleaned_data.get('guardian_1_email')
        user.save() # Save to the user table
        # Create student database item attached to that user.
        student = Student.objects.create(
            user=user,
            guardian1_firstname=self.cleaned_data.get('guardian_1_first_name'),
            guardian1_lastname=self.cleaned_data.get('guardian_1_last_name'),
            guardian1_email=self.cleaned_data.get('guardian_1_email'),
            guardian1_phone=self.cleaned_data.get('guardian_1_phone'),
            guardian2_firstname=self.cleaned_data.get('guardian_2_first_name'),
            guardian2_lastname=self.cleaned_data.get('guardian_2_last_name'),
            guardian2_email=self.cleaned_data.get('guardian_2_email'),
            guardian2_phone=self.cleaned_data.get('guardian_2_phone'),
            emergency_contact_firstname=self.cleaned_data.get('guardian_1_first_name'),
            emergency_contact_lastname=self.cleaned_data.get('guardian_1_last_name'),
            emergency_contact_email=self.cleaned_data.get('guardian_1_email'),
            emergency_contact_phone=self.cleaned_data.get('guardian_1_phone')
        ) # Create user instance for student model.
        
        student.save()
        # Return created user.
        return user

# Forms for registering teacher
class TeacherCreationForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    subject = forms.CharField(required=True)

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email', 'first_name', 'last_name', 'subject',) #Password auto added since required

    @transaction.atomic
    def save(self):
        # Create user and set user specific attributes.
        user = super().save(commit=False) # Saving instance of our form.
        # Set commit to false so we can save the below info before committing to database.
        user.is_teacher = True
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save() # Save to the user table
        # Create teacher database item attached to that user.
        teacher = Teacher.objects.create(user=user) # Create user instance for teacher model.
        teacher.subject = self.cleaned_data.get('subject')
        teacher.save()
        # Return created user.
        return user

# Forms for registering parent
class ParentCreationForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    job = forms.CharField(required=True)

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email', 'first_name', 'last_name', 'job',) #Password auto added since required

    @transaction.atomic
    def save(self):
        # Create user and set user specific attributes.
        user = super().save(commit=False) # Saving instance of our form.
        # Set commit to false so we can save the below info before committing to database.
        user.is_parent = True
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save() # Save to the user table
        # Create teacher database item attached to that user.
        parent = Parent.objects.create(user=user) # Create user instance for parent model.
        parent.job = self.cleaned_data.get('job')
        parent.save()
        # Return created user.
        return user