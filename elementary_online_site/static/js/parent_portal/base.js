var mini = true;

function toggleSidebar() {
  if (mini) {
    console.log("opening sidebar");
    document.getElementById("sidebar").style.width = "20vw";
    document.getElementById("main").style.marginLeft = "14vw";
    this.mini = false;
  } 
  else {
    console.log("closing sidebar");
    document.getElementById("sidebar").style.width = "5vw";
    document.getElementById("main").style.marginLeft = "0vw";
    this.mini = true;
  }
}