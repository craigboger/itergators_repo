var mini = true;

function toggleSidebar() {
  if (mini) {
    console.log("opening sidebar");
    document.getElementById("sidebar").style.width = "275px";
    document.getElementById("main").style.width = "-275px";
    this.mini = false;
  } 
  else {
    console.log("closing sidebar");
    document.getElementById("sidebar").style.width = "85px";
    document.getElementById("main").style.width = "-85px";
    this.mini = true;
  }
}
