from django.apps import AppConfig


class ParentPortalConfig(AppConfig):
    name = 'parent_portal'
