# teacher_portal/tests/view.py

'''
This is mainly for route testing to ensure a user sees the appropriate
pages while navigating the teacher admin portal or classroom.
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Model Imports
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Files
from pathlib import Path
from django.core.files import File
import os
import glob
import shutil

# Import other Base Test cases to build off of
from classroom.tests.view import BaseTestCase # Base test case for setting up new database with student, teacher, classroom, assignment, etc

# TESTS
# ------------------------------------------------------------------------------


class TeacherClassroomAdminPageTest(BaseTestCase):
    '''
    Class to perform tests that make sure classroom admin page loading 
    and urls are good.
    Routing to the teacher admin portal already tested in accounts.tests.view
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login teacher and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_view_url_exists_at_proper_location(self):
        link = '/teacher_portal/'
        resp = self.client.get(link, follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'teacher_portal/portal.html')


class TeacherSubjectAdminPageTest(BaseTestCase):
    '''
    Class to perform tests that make sure subject admin page loading 
    and urls are good.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login teacher and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_view_url_exists_at_proper_location(self):
        link = '/teacher_portal/admin_subject_id_' + str(self.subject_1.id) + '/'
        resp = self.client.get(link, follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id,)), follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id,)), follow=True)
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'teacher_portal/subject_portal.html')


class TeacherClassAdminNavigationTest(BaseTestCase):
    '''
    Class to perform tests that emulate teacher navigating through a
    generated classroom admin page.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_subject_menu_contents(self):
        '''
        Test to make sure the menu for the subject admin pages shows all
        subject associated with the classroom.
        '''
        # Navigate to the classroom admin page after login
        response = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm button links to subject pages exist on the page
        subject_1_button_link = f'/teacher_portal/admin_subject_id_{self.subject_1.id}/'
        subject_2_button_link = f'/teacher_portal/admin_subject_id_{self.subject_2.id}/'
        self.assertContains(response, subject_1_button_link, html=False)
        self.assertContains(response, subject_2_button_link, html=False)
        # Confirm that pressing the subject 1 button goes to the appropriate subject page
        # Buttons are simple link, so just use get request.
        sub1_response = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id,)), follow=True)
        self.assertEqual(sub1_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(sub1_response, 'teacher_portal/subject_portal.html') 
        self.assertContains(sub1_response, str(self.subject_1.name), html=True) 

    def test_weekly_class_content_card_contents(self):
        '''
        Test to make sure the contents of the weekly class contents card
        are what they should be.
        '''
        # Navigate to the classroom admin page after login
        response = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of week header and week card
        self.assertContains(
            response,
            'WEEKLY CLASSROOM CONTENT',
            html=False
        )
        self.assertContains(
            response,
            str(self.test_weekly_class_content_wk1.week_date_header),
            html=False
        )

    def test_class_content_card_contents(self):
        '''
        Test to make sure the contents of the class contents card
        are what they should be.
        '''
        # Navigate to the classroom admin page after login
        response = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of class update current week and class card
        self.assertContains(
            response,
            'CLASSROOM CONTENT',
            html=False
        )
        self.assertContains(
            response,
            'Update Current Week',
            html=False
        )
        self.assertEqual(1,1)

    def test_create_new_week_card_contents(self):
        '''
        Test to make sure the contents of the create new week card
        are what they should be.
        '''
        # Navigate to the classroom admin page after login
        response = self.client.get(reverse('teacher_portal'), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of class update current week and class card
        self.assertContains(
            response,
            'CREATE NEW WEEK',
            html=False
        )
        self.assertContains(
            response,
            'Class header',
            html=False
        )

    def test_change_class_week_to_administer(self):
        '''
        Test to see if changing weeks to administer will work.
        '''
        # Simulate requesting week 2 from post request
        response = self.client.post(reverse('teacher_portal'), {'week_wanted': '2', 'week_wanted_submit':''}, follow=True)
        # Test if redirect successful
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Test that week 2 is showing.
        self.assertContains(
            response, 
            self.test_weekly_class_content_wk2.week_date_header, 
            html=False
        )


class TeacherSubjectAdminNavigationTest(BaseTestCase):
    '''
    Class to perform tests that emulate teacher navigating through a 
    generated subject admin page.
    '''

    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_weekly_subject_content_card_contents(self):
        '''
        Test to make sure the contents of the weekly subject contents card
        are what they should be.
        '''
        # Navigate to the subject admin page after login
        response = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id, 1)), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of week header and week card
        self.assertContains(
            response,
            'WEEKLY SUBJECT PAGE CONTENT',
            html=True
        )
        self.assertContains(
            response,
            str(self.sub1_wk1.week_date_header),
            html=False
        )

    def test_weekly_assignment_management_content_card_contents(self):
        '''
        Test to make sure the contents of the weekly assignment management card
        are present.  Confirm presence of assignments and the create, update, delete buttons
        '''
        # Navigate to the subject admin page after login
        response = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id, 1)), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of assignments header and assignments
        self.assertContains(
            response,
            'MANAGE WEEK ASSIGNMENTS',
            html=True
        )
        self.assertContains(
            response,
            str(self.sub1_wk1_assn1.title),
            html=True
        )
        self.assertContains(
            response,
            str(self.sub1_wk1_assn2.title),
            html=True
        )
        # Confirm presence of create, update, delete buttons
        self.assertContains(
            response,
            '>Delete</button></td>',
            html=False
        )
        self.assertContains(
            response,
            '>Update</button></td>',
            html=False
        )
        self.assertContains(
            response,
            '>Create Assignment</button>',
            html=False
        )

    def test_student_list_content_card_contents(self):
        '''
        Test to make sure the contents of the student list card are correct
        '''
        # Navigate to the subject admin page after login
        response = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id, 1)), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of student list card header and student in list
        self.assertContains(
            response,
            'STUDENT LIST',
            html=True
        )
        self.assertContains(
            response,
            str(self.test_student.user.first_name),
            html=True
        )
        self.assertContains(
            response,
            str(self.test_student.user.last_name),
            html=True
        )

    def test_grading_queue_content_card_contents(self):
        '''
        Test to make sure the contents of the grading queue are correctly populated.
        Also make sure grading button is present.
        '''
        # Navigate to the subject admin page after login
        response = self.client.get(reverse('subject_admin_page', args=(self.subject_1.id, 1)), follow=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Confirm presence of student list card header and student in list
        self.assertContains(
            response,
            'GRADING QUEUE',
            html=True
        )
        self.assertContains(
            response,
            str(self.sub1_wk1_assn3_submitted.source_assignment.title),
            html=True
        )
        self.assertContains(
            response,
            str(self.test_student.user.first_name),
            html=True
        )
        self.assertContains(
            response,
            str(self.test_student.user.last_name),
            html=True
        )
        self.assertContains(
            response,
            'Grade</button>',
            html=False
        )

    def test_change_subject_week_to_administer(self):
        '''
        Test to see if changing weeks to administer will work.
        '''
        # Simulate requesting week 2 from post request
        response = self.client.post(reverse('subject_admin_page', args=(self.subject_1.id,)), {'week_wanted': '2', 'week_wanted_submit':''}, follow=True)
        # Test if redirect successful
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # Test that week 2 is showing.
        self.assertContains(
            response, 
            self.sub1_wk2.week_date_header, 
            html=False
        )