# teacher_portal/tests/view.py

'''
This is mainly for route testing submission forms and the resulting models
they create in the database
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Model Imports
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Files
from pathlib import Path
from django.core.files import File
import os
import glob
import shutil

# Import other Base Test cases to build off of
from classroom.tests.view import BaseTestCase # Base test case for setting up new database with student, teacher, classroom, assignment, etc

# TESTS
# ------------------------------------------------------------------------------


class ClassAdminPageFormsTest(BaseTestCase):
    '''
    This class tests the forms on the classroom admin page.
    '''
    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login teacher and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        pass

    def test_updating_class_week_contents_form(self):
        '''
        Test of updating the week date header for week 1 of the classroom
        '''
        # Simulate submitting new weekly class content to the teacher
        # admin portal
        update_response = self.client.post(
            reverse('teacher_portal', args=(1,)), 
            {
                'week_date_header': 'This is an updated header!', 
                'whiteboard_link': 'https://www.google45.com',
                'week_class_contents_update':''
            }, 
            follow=True
        )
        # Make sure we direct to the teacher portal after submission
        self.assertEqual(update_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(update_response, 'teacher_portal/portal.html')
        # Get the classroom and check the contents match what is was
        # updated to.
        curr_classroom_wk_content = WeeklyClassroomContent.objects.filter(
            source_class=self.test_classroom
        ).first()
        self.assertEqual(
            str(curr_classroom_wk_content.week_date_header),
            'This is an updated header!'
        )
        self.assertEqual(
            str(curr_classroom_wk_content.whiteboard_link),
            'https://www.google45.com'
        )

    def test_updating_class_contents_form(self):
        '''
        Test of updating the zoom links, and availability status.
        '''
        # Simulate submitting new class content to the teacher admin portal
        update_response = self.client.post(
            reverse('teacher_portal', args=(1,)), 
            data={
                'class_zoom_link': 'https://ufl.zoom.us/j/93347269702',
                'teacher_zoom_link': 'https://ufl.zoom.us/j/93347269702',
                'tutor_zoom_link': 'https://ufl.zoom.us/j/93347269702',
                'class_available': True,
                'teacher_available': True,
                'tutor_available': True,
                'class_contents_update':''
            }, 
            follow=True
        )
        # Make sure we direct to the teacher portal after submission
        self.assertEqual(update_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(update_response, 'teacher_portal/portal.html')
        # Get current classroom and check the contents
        curr_classroom = Classroom.objects.all().first()
        self.assertEqual(
            str(curr_classroom.class_zoom_link),
            'https://ufl.zoom.us/j/93347269702'
        )
        self.assertEqual(
            str(curr_classroom.teacher_zoom_link),
            'https://ufl.zoom.us/j/93347269702'
        )
        self.assertEqual(
            str(curr_classroom.tutor_zoom_link),
            'https://ufl.zoom.us/j/93347269702'
        )
        self.assertEqual(
            curr_classroom.class_available,
            True
        )
        self.assertEqual(
            curr_classroom.teacher_available,
            True
        )
        self.assertEqual(
            curr_classroom.tutor_available,
            True
        )

    def test_changing_current_week(self):
        '''
        Test to make sure we can successfully change current week
        students will see when logging in.
        '''
        # Simulate submitting new class week to the teacher admin portal
        change_week_response = self.client.post(
            reverse('teacher_portal', args=(1,)), 
            data={
                'curr_week_wanted':'2',
                'set_current_week':''
            }, 
            follow=True
        )
        # Make sure we direct to the teacher portal after submission
        self.assertEqual(change_week_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(change_week_response, 'teacher_portal/portal.html')
        # Get current classroom and check the current week
        curr_classroom = Classroom.objects.all().first()
        self.assertEqual(
            curr_classroom.current_week,
            2
        )

    def test_creating_new_week_form(self):
        '''
        Test of creating new week.  Need to s
        '''
        # Simulate submitting request to create new week of classes and subjects
        new_class_weeks_response = self.client.post(
            reverse('teacher_portal', args=(1,)), 
            data={
                'class_week_number':'3',
                'class_header': 'This is a test new week 3!',
                'whiteboard_link': 'https://ufl.zoom.us/j/93347269702',
                'create_new_week':''
            }, 
            follow=True
        )
        # Make sure we direct to the teacher portal after submission
        self.assertEqual(new_class_weeks_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(new_class_weeks_response, 'teacher_portal/portal.html')
        # Get the new class week and test the content
        new_class_week = WeeklyClassroomContent.objects.filter(week_number=3).first()
        self.assertEqual(
            new_class_week.week_date_header,
            'This is a test new week 3!'
        )
        self.assertEqual(
            new_class_week.whiteboard_link,
            'https://ufl.zoom.us/j/93347269702'
        )
        # Get the new week for subject 1 and test contents
        new_week_sub1 = WeeklySubjectContent.objects.filter(
            week_number=3, 
            source_subject=self.subject_1,
        ).first()
        self.assertEqual(
            new_week_sub1.week_date_header,
            'This is a test new week 3!'
        )
        self.assertEqual(
            new_week_sub1.whiteboard_link,
            'https://ufl.zoom.us/j/93347269702'
        )
        # Get the new week for subject 2 and test contents
        new_week_sub2 = WeeklySubjectContent.objects.filter(
            week_number=3, 
            source_subject=self.subject_2,
        ).first()
        self.assertEqual(
            new_week_sub2.week_date_header,
            'This is a test new week 3!'
        )
        self.assertEqual(
            new_week_sub2.whiteboard_link,
            'https://ufl.zoom.us/j/93347269702'
        )


class SubjectAdminFormTest(BaseTestCase):
    '''
    This class tests the forms on the subject admin page.
    '''
    def setUp(self):
        # Call base setup and add custom setup here
        super().setUp()
        # Go ahead and login teacher and make sure teacher successfully logged in.
        user = self.client.login(username=self.teacher_login_credentials['username'], password=self.teacher_login_credentials['password'])
        self.assertTrue(user)

    def tearDown(self):
        super().tearDown()
        # In addition to the files removed in the base test class, need to 
        # delete assignment files in this test.
        test_dir = Path(__file__).parent.absolute()
        proj_dir = Path(__file__).parents[2].absolute()
        created_assn_static_dir = Path(proj_dir, 'static', 'uploaded_files', 'created_assignments')
        search_folder = str(created_assn_static_dir) + '/Test_Classroom'

        try:
            shutil.rmtree(search_folder)
        except OSError as e:
            pass

    def test_updating_subject_week_contents_form(self):
        '''
        Test of updating the week date header for week 1 of the classroom
        '''
        # Simulate submitting new weekly subject content to the teacher
        # admin portal
        update_response = self.client.post(
            reverse('subject_admin_page', args=(self.subject_1.id, 1)), 
            {
                'week_date_header': 'This is an updated header!', 
                'whiteboard_link': 'https://www.google45.com',
                'week_subject_contents_update':''
            }, 
            follow=True
        )
        # Make sure we direct to the subject admin page after submission
        self.assertEqual(update_response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(update_response, 'teacher_portal/subject_portal.html')
        # Get the classroom and check the contents match what is was
        # updated to.
        curr_subject_wk_content = WeeklySubjectContent.objects.filter(
            source_subject=self.subject_1,
            week_number=1
        ).first()
        self.assertEqual(
            str(curr_subject_wk_content.week_date_header),
            'This is an updated header!'
        )
        self.assertEqual(
            str(curr_subject_wk_content.whiteboard_link),
            'https://www.google45.com'
        )

    def test_creating_new_assignment_form(self):
        '''
        Test to see that creation of assignment form works:
        1) Create assignment by submitting post to form
        2) Make sure we redirect to the subject page
        3) Make sure the new assignment is on the list.
        '''
        # Making direct post request to form since we already know the 
        # create assignment button is present.
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','031-papyrus.png'), "rb")
        submit_response = self.client.post(
            reverse('create_assignment_modal', args=(self.sub1_wk1.id,)),
            {
                'title': 'A New Test Assignment',
                'description': 'A new assignment to test',
                'assignment_file':f1,
            },
            follow=True
        )
        f1.close()
        # Confirm the user was redirected to the subject page again.
        self.assertTemplateUsed(submit_response, 'teacher_portal/subject_portal.html')
        # Grab the created assignment to confirm details.
        created_assignment = Assignment.objects.filter(
            source_weekly_subject_content=self.sub1_wk1,
            title='A New Test Assignment',
            description='A new assignment to test'
        ).first()
        # Confirm assignment now exists.
        self.assertTrue(created_assignment)
        # Confirm assignment is also listed on the assignment page now.
        self.assertContains(
            submit_response,
            created_assignment.title,
            html=False
        )

    def test_update_assignment_form(self):
        '''
        Test to make sure assignment editing form functions.
        '''
        # Making direct post request to form since we already know the 
        # create assignment button is present.
        test_dir = Path(__file__).parent.absolute()
        f1 = open(Path(test_dir,'files_for_tests', 'subject_page_logos','abacus.png'), "rb")
        submit_response = self.client.post(
            reverse('teacher_update_assignment_modal', args=(self.sub1_wk1.id, self.sub1_wk1_assn1.id)),
            {
                'title': 'A Update Test Assignment',
                'description': 'A update assignment to test',
                'assignment_file':f1,
            },
            follow=True
        )
        f1.close()
        # Confirm the user was redirected to the subject page again.
        self.assertTemplateUsed(submit_response, 'teacher_portal/subject_portal.html')
        # Grab the updated assignment to confirm details.
        updated_assignment = Assignment.objects.filter(
            source_weekly_subject_content=self.sub1_wk1,
            title='A Update Test Assignment',
            description='A update assignment to test'
        ).first()
        # Confirm assignment with new details now exists
        self.assertTrue(updated_assignment)
        # Confirm updated assignment title is now listed on the page
        self.assertContains(
            submit_response,
            updated_assignment.title,
            html=False
        )

    def test_delete_assignment_form(self):
        '''
        Test to make sure assignment delting assignment form functions.
        '''
        # Making direct post request to form since we already know the 
        # delete assignment button is present.
        delete_response = self.client.post(
            reverse('delete_assignment_modal', args=(self.sub1_wk1.id, self.sub1_wk1_assn1.id)),
            {'submit': 'Confirm',},
            follow=True
        )
        # Confirm the user was redirected to the subject page again.
        self.assertTemplateUsed(delete_response, 'teacher_portal/subject_portal.html')
        # Confirm the subject admin page no longer lists the assignment
        self.assertNotContains(
            delete_response,
            str(self.sub1_wk1_assn1.title)
        )

    def test_grading_submitted_assignment_form(self):
        '''
        Test of grading an assignment in the grading queue.
        '''
        # Making direct post request to form since we already know the 
        # grade assignment button is present.
        grade_response = self.client.post(
            reverse('grade_assignment_assignment_modal', args=(self.sub1_wk1_assn3_submitted.id,)),
            {
                'grade': '95',
                'feedback': 'Good work!'
            },
            follow=True
        )
        # Confirm the user was redirected to the subject page again.
        self.assertTemplateUsed(grade_response, 'teacher_portal/subject_portal.html')
        # Grab the graded assignment to confirm details.
        graded_assignment = SubmittedAssignment.objects.filter(
            source_assignment=self.sub1_wk1_assn3,
            source_student=self.test_student,
        ).first()
        # Confirm assignment graded
        self.assertEqual(
            graded_assignment.grade,
            95
        )
        self.assertEqual(
            graded_assignment.feedback,
            'Good work!'
        )
        self.assertTrue(graded_assignment.is_graded)
        # Confirm the graded assignment no longer in the grading queue
        # Confirm updated assignment title is now listed on the page
        self.assertContains(
            grade_response,
            graded_assignment.source_assignment.title,
            html=False
        )


