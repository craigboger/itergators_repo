# classroom/admin.py

from django.contrib import admin

# Import your models here.
from .models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)


# Create model admins to use with model registration.

class ClassroomAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher')
    list_filter = ('teacher','name')

class WeeklyClassroomContentAdmin(admin.ModelAdmin):
    list_display = ('source_class', 'week_number')
    list_filter = ('source_class', 'week_number')

class SubjectPageAdmin(admin.ModelAdmin):
    list_display = ('source_class', 'name')
    list_filter = ('source_class', 'name')

class WeeklySubjectPageContentAdmin(admin.ModelAdmin):
    list_display = ('class_name', 'subject_name', 'week_number')
    list_filter = ('week_number',)

class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('title', 'class_name', 'subject_name', 'assignment_week')
    list_filter = ('title',)

class SubmittedAssignmentAdmin(admin.ModelAdmin):
    list_display = (
        'assignment_name', 
        'class_name', 
        'subject_name', 
        'assignment_week', 
        'associated_student_last_name', 
        'associated_student_first_name'
    )

# Register models here.
admin.site.register(Classroom, ClassroomAdmin)
admin.site.register(WeeklyClassroomContent, WeeklyClassroomContentAdmin)
admin.site.register(SubjectPage, SubjectPageAdmin)
admin.site.register(WeeklySubjectContent, WeeklySubjectPageContentAdmin)
admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(SubmittedAssignment, SubmittedAssignmentAdmin)

# admin.site.register(
#     (
#         Classroom,
#         WeeklyClassroomContent,
#         SubjectPage,
#         WeeklySubjectContent,
#         Assignment,
#         SubmittedAssignment
#     )
# )