# classroom/views.py

"""
Notes:

Render view based on dropdown.
https://stackoverflow.com/questions/24246995/render-a-different-view-based-on-drop-down-menu-selection

Create dependent menus based on Ajax request
https://simpleisbetterthancomplex.com/tutorial/2018/01/29/how-to-implement-dependent-or-chained-dropdown-list-with-django.html

"""

# IMPORTS
# ------------------------------------------------------------------------------
# For URL coordination and redirect
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse, reverse_lazy
from django import http

# For login
from django.contrib.auth import (
    authenticate,
    login,
    logout
)

# For message responses
from django.contrib import messages

# For Authorization
from django.contrib.auth.decorators import login_required

# Import models to use
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
    Parent
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent,
    Assignment,
    SubmittedAssignment
)

# Import decorators for authorization and checks
from accounts.decorators import (
    unauthenticated_user, 
    #allowed_users, 
    #student_only,
    student_required,
    teacher_required,
    parent_required,
    student_or_teacher_required
)

# Import input forms to use
#from .forms import CreateUserForm
from classroom.forms import (
    StudentSubmitAssignmentForm,
    StudentUpdateAssignmentForm,
)
from bootstrap_modal_forms.generic import BSModalCreateView # For creating modal form views
from bootstrap_modal_forms.generic import BSModalFormView
from bootstrap_modal_forms.generic import BSModalUpdateView

# For aggregations and calculations
from django.db.models import Avg

# VIEWS
# ------------------------------------------------------------------------------
@login_required(login_url='login')  # If user not logged in, go to login page
@student_or_teacher_required
def NoAssociatedClassView(request):
    return render(request, 'classroom/no_associated_class.html')

@login_required(login_url='login')  # If user not logged in, go to login page
@student_or_teacher_required
def ClassroomView(request, week_wanted=None):
    # local vars
    curr_user = None
    curr_student = None
    curr_teacher = None
    curr_classroom = None  
    teacher_name = None
    # Get the user accessing the page.
    curr_user = request.user
    # If student, grab classroom associated with the student.
    if (curr_user.is_student):
        curr_student = Student.objects.get(user__id=curr_user.id)
        curr_classroom = curr_student.stud_class
    # If teacher, grab classroom associated with the teacher.
    # TODO: Need to update logic later to handle the teacher.  Stick with using curr_classroom
    else:
        curr_teacher = Teacher.objects.get(user__id=curr_user.id)
        curr_classroom = curr_teacher.classroom_set.first()
    # Grab classroom data associated with the user.
    teacher_name = curr_classroom.teacher.user  # Use teacher_name.first_name / teacher_name.last_name
    # Grab list of subjects associated with the classroom.
    curr_class_subjects = curr_classroom.subjectpage_set.all().order_by('name')
    # Grab zoom links associated with the classroom
    class_zoom_link = curr_classroom.class_zoom_link
    teacher_zoom_link = curr_classroom.teacher_zoom_link
    tutor_zoom_link = curr_classroom.tutor_zoom_link
    # Grab availability statuses for the different Zoom rooms.
    class_available = curr_classroom.class_available
    teacher_available = curr_classroom.teacher_available
    tutor_available = curr_classroom.tutor_available
    # Use subject page ID to create dynamic URL in HTML
    # Grab weekly content associated with the class
    week_classroom_contents = curr_classroom.weeklyclassroomcontent_set.all().order_by('week_number')
    # Get the current week, redirect if different week selected
    # Can be week from url, week from dropdown, or default week from teacher admin
    if request.method == 'POST':
        week_wanted = request.POST.get('week_wanted')
        return redirect(reverse(ClassroomView, args=(week_wanted,)))
    elif week_wanted == None:
        week_wanted = curr_classroom.current_week
        return redirect(reverse(ClassroomView, args=(week_wanted,)))
    else:
        week_wanted = week_wanted
    #Set current week for comparison in selection
    current_week = curr_classroom.weeklyclassroomcontent_set.get(week_number=week_wanted)
    # Assemble context to provide to view.
    context = {
        'teacher_name':teacher_name,
        'class_subjects':curr_class_subjects,
        'class_zoom_link':class_zoom_link,
        'teacher_zoom_link':teacher_zoom_link,
        'tutor_zoom_link':tutor_zoom_link,
        'class_available':class_available,
        'teacher_available':teacher_available,
        'tutor_available':tutor_available,
        'week_classroom_contents':week_classroom_contents,
        'current_week':current_week
    }
    return render(request, 'classroom/classroom.html', context)

@login_required(login_url='login')  # If user not logged in, go to login page
@student_or_teacher_required
def SubjectPageView(request):
    # Get subject page referenced by the dynamic url
    context = {}
    return render(request, 'classroom/subject_page.html')

@login_required(login_url='login')  # If user not logged in, go to login page
@student_or_teacher_required
def SubjectPageDynView(request, pk_subjectpage, week_wanted=None):
    # Get subject page referenced by the dynamic url
    req_subject_page = SubjectPage.objects.get(id=pk_subjectpage)
    # Get the weekly content associated with the subject page
    week_subject_contents = req_subject_page.weeklysubjectcontent_set.all().order_by('week_number')
    # Get current week requested
    if request.method == 'POST':
        week_wanted = request.POST.get('week_wanted')
        return redirect(reverse(SubjectPageDynView, args=(pk_subjectpage, week_wanted)))
    elif week_wanted == None:
        week_wanted = req_subject_page.source_class.current_week
    else:
        week_wanted = week_wanted
    # Grab week wanted from classroom
    # print("------- Getting current week from week wanted.")
    # print(week_wanted)
    # print(req_subject_page.weeklysubjectcontent_set)
    current_week = req_subject_page.weeklysubjectcontent_set.get(week_number=week_wanted)
    curr_week_assignments = current_week.assignment_set.all().order_by('title')
    # Get all assignments for the current subject that the user has submitted
    curr_user = request.user
    submitted_assignments = SubmittedAssignment.objects.filter(source_student__user__id=curr_user.id, source_assignment__source_weekly_subject_content__source_subject__id=req_subject_page.id)
    # Get all assignments the student did not submit
    assigments_that_were_submitted = submitted_assignments.select_related('source_assignment').values('source_assignment_id')
    #assignments_for_subject = Assignment.objects.filter(source_weekly_subject_content__source_subject__id=req_subject_page.id)
    # assignments_not_submitted = Assignment.objects.exclude(id__in=assigments_that_were_submitted)
    assignments_not_submitted = curr_week_assignments.exclude(id__in=assigments_that_were_submitted)
    context = {
        'week_subject_contents':week_subject_contents,
        'current_week':current_week,
        'subject_contents':req_subject_page,
        'curr_week_assignments':curr_week_assignments,
        'assignments_not_submitted':assignments_not_submitted
    }
    return render(request, 'classroom/subject_page.html', context)

@login_required(login_url='login')  # If user not logged in, go to login page
@student_or_teacher_required
def AssignmentsPageView(request, pk_subjectpage):
    # Get the current user accessing the page
    curr_user = request.user
    curr_student = Student.objects.get(user__id=curr_user.id)
    # Get the current subject based on the primary key provided
    curr_subject = SubjectPage.objects.get(id=pk_subjectpage)
    curr_class = curr_subject.source_class
    # Get all assignments associated with the subject
    assignments_for_subject = Assignment.objects.filter(source_weekly_subject_content__source_subject__id=pk_subjectpage)
    # Get all assignments for the current subject that the user has submitted
    submitted_assignments = SubmittedAssignment.objects.filter(source_student__user__id=curr_student.user.id, source_assignment__source_weekly_subject_content__source_subject__id=curr_subject.id)
    # Get all assignments the student did not submit
    assigments_id_that_were_submitted = submitted_assignments.select_related('source_assignment').values('source_assignment_id')
    assignments_not_submitted = Assignment.objects.exclude(id__in=assigments_id_that_were_submitted)
    # Get a list of weeks that have submitted assignments
    #assigments_that_were_submitted = submitted_assignments.select_related('source_assignment').values('source_assignment_id')
    #weeks_with_submitted_assignments = submitted_assignments.values('source_assignment__source_weekly_subject_content__week_number')
    # Getting assignment objects of those assignments that were submitted
    assignments_submitted = Assignment.objects.filter(id__in=assigments_id_that_were_submitted)
    # Getting weekly subject content objects of those assignments submitted
    weekly_subject_ids_with_submitted_assignments = assignments_submitted.select_related('source_weekly_subject_content').values('source_weekly_subject_content_id')
    # Filter down to weekly subjects that have submitted assignments for the student.
    weekly_subject_content_of_submitted_assignments = WeeklySubjectContent.objects.filter(id__in=weekly_subject_ids_with_submitted_assignments)
    # Extract unique week numbers from set
    weeks_with_submitted_assignments = weekly_subject_content_of_submitted_assignments.order_by('week_number').values('week_number').distinct()
    # print("-----------------Printing out weeks with submitted assignments")
    # print(weeks_with_submitted_assignments)
    # Get average grade for all submitted assignments
    student_average = submitted_assignments.aggregate(Avg('grade'))
    # Round if the average is available.
    if student_average['grade__avg']:
        student_average = round(student_average['grade__avg'], 2)
    else:
        student_average = student_average['grade__avg']
    context = {
        'curr_student':curr_student,
        'curr_subject':curr_subject,
        'curr_class':curr_class,
        'assignments_for_subject':assignments_for_subject,
        'submitted_assignments':submitted_assignments,
        'unsubmitted_assignments':assignments_not_submitted,
        'weeks_with_submitted_assignments':weeks_with_submitted_assignments,
        'student_average':student_average
    }
    return render(request, 'classroom/assignments_page.html', context)

# View to use in modal form for submitting a new assignment
class SubmitAssignmentView(BSModalFormView):
    template_name = 'classroom/assignment_submit.html'
    form_class = StudentSubmitAssignmentForm
    success_message = 'Success: Assignment Submitted.'   
    #success_url = reverse_lazy('home')
    
    # Function to redirect to the custom assignment page url on successful 
    # submission of form.
    def get_success_url(self):
        curr_assignment_id = self.kwargs['pk_assignment']
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_subject = curr_assignment.source_weekly_subject_content.source_subject
        week_num_wanted = curr_assignment.source_weekly_subject_content.week_number
        return reverse(SubjectPageDynView, args=(curr_subject.id, week_num_wanted))
        #return reverse_lazy('home')   

    def form_valid(self, form):
        if not self.request.is_ajax():
            curr_assignment_id = self.kwargs['pk_assignment']
            curr_assignment = Assignment.objects.get(id=curr_assignment_id)
            # print("-----CURRENT ASSIGNMENT FOUND")
            # print(curr_assignment)
            self.object = form.save(self.request.user, curr_assignment)
        # return super(SubmitAssignmentView, self).form_valid(form)
        # return redirect(self.get_success_url())
        return http.HttpResponseRedirect(self.get_success_url())

    # Function to get the current assignment in the view.
    def get_curr_assignment(self):
        curr_assignment_id = self.kwargs['pk_assignment']
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        return curr_assignment

    # Overload this function to get extra context info to template in
    # class based view.
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        curr_assignment_id = self.kwargs['pk_assignment']
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_user_id = self.kwargs['pk_user']
        curr_user = CustomUser.objects.get(id=curr_user_id)
        # Grab already submitted assignment if it exists
        try:
            curr_submitted_assignment = SubmittedAssignment.objects.get(source_assignment__id=curr_assignment.id, source_student__user__id=curr_user.id)
        except SubmittedAssignment.DoesNotExist:
            curr_submitted_assignment = None
        # Add to context
        context['curr_assignment'] = curr_assignment
        context['curr_user'] = curr_user
        context['curr_submitted_assignment'] = curr_submitted_assignment
        return context

# View to use in modal form for updating a submitted assignment
class StudentUpdateAssignmentView(BSModalUpdateView):
    template_name = 'classroom/student_update_assignment.html'
    form_class = StudentUpdateAssignmentForm
    success_message = 'Success: Assignment Updated.'
    model = SubmittedAssignment

    # Function to redirect to the custom assignment page url on successful 
    # submission of form.
    def get_success_url(self):
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_subject = curr_assignment.source_weekly_subject_content.source_subject
        return reverse(AssignmentsPageView, args=(curr_subject.id,))
        #return reverse_lazy('home')   

    # Function to get the current assignment in the view.
    def get_curr_assignment(self):
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        return curr_assignment

    # Overload this function to get extra context info to template in
    # class based view.
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        curr_submitted_assignment_id = self.kwargs['pk_submitted_assignment']
        curr_submitted_assignment = SubmittedAssignment.objects.get(id=curr_submitted_assignment_id)
        curr_assignment_id = curr_submitted_assignment.source_assignment.id
        curr_assignment = Assignment.objects.get(id=curr_assignment_id)
        curr_user_id = self.kwargs['pk_user']
        curr_user = CustomUser.objects.get(id=curr_user_id)
        # Add to context
        context['curr_assignment'] = curr_assignment
        context['curr_user'] = curr_user
        context['curr_submitted_assignment'] = curr_submitted_assignment
        return context