# accounts/models.py

'''
Creating a custom user:
https://rakibul.net/django-custom-user-model-auth

Custom user model and permissions options:
https://simpleisbetterthancomplex.com/tutorial/2018/01/18/how-to-implement-multiple-user-types-with-django.html

Phone Number Field:
https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models (phonenumberfield)
https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models (using regex)

'''

from django.db import models

# User models to inherit from.
from django.contrib.auth.models import AbstractUser, UserManager
from phonenumber_field.modelfields import PhoneNumberField

# Custom user manager
class CustomUserManager(UserManager):
    pass

# Custom user model.  
class CustomUser(AbstractUser):
    # Boolean flags to handle user authorization of pages
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_parent = models.BooleanField(default=False)
    # Name Attributes
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name + " " + self.last_name

class Parent (models.Model):
    """
    Model to handle data specific to a student.
    """
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    job = models.CharField(max_length=100)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

class Student (models.Model):
    """
    Model to handle data specific to a student.
    """
    # Student Info
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    birth_date = models.DateField(null=True)
    # Guardian Info
    guardian1_firstname = models.CharField(blank=False, null=True, max_length=254)
    guardian1_lastname = models.CharField(blank=False, null=True, max_length=254)
    guardian1_email = models.EmailField(blank=False, null=True, max_length=254)
    guardian1_phone = PhoneNumberField(blank=False, null=True, unique=False)
    guardian2_firstname = models.CharField(blank=True, null=True, max_length=254)
    guardian2_lastname = models.CharField(blank=True, null=True, max_length=254)
    guardian2_email = models.EmailField(blank=True, null=True, max_length=254)
    guardian2_phone = PhoneNumberField(blank=True, null=True, unique=False)
    # Emergency Contact Info
    emergency_contact_firstname = models.CharField(null=True, max_length=254)
    emergency_contact_lastname = models.CharField(null=True, max_length=254)
    emergency_contact_email = models.EmailField(null=True, max_length=254)
    emergency_contact_phone = PhoneNumberField(null=True, blank=False, unique=False)
    # Relationship to parent user
    stud_parent = models.ForeignKey(Parent, on_delete=models.SET_NULL, null=True, blank=True)
    # Relationship to classroom the student is enrolled in
    stud_class = models.ForeignKey("classroom.Classroom", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

class Teacher (models.Model):
    """
    Model to handle data specific to a teacher.
    """
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    office_phone = PhoneNumberField(null=True, blank=False, unique=True)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

