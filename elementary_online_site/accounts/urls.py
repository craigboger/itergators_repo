# accounts/urls.py

from django.urls import path
from .views import (
    loginPage,
    logoutUser,
    student_register,
    teacher_register,
    parent_register,
)

urlpatterns = [
    path('login/', loginPage, name='login'),
    path('register_student/', student_register, name='register_student'),
    path('register_teacher/', teacher_register, name='register_teacher'),
    path('register_parent/', parent_register, name='register_parent'),
    path('logout/', logoutUser, name='logout'),
]