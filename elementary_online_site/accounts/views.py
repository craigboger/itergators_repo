# IMPORTS
# ------------------------------------------------------------------------------
# For URL coordination and redirect and view creation
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import CreateView
from django.urls import reverse_lazy

# For login
from django.contrib.auth import (
    authenticate,   # Authenticate a user
    login,          # Login a user
    logout          # Logout a user
)

# For message responses
from django.contrib import messages

# For Authorization
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

# Import models to use
from .models import (
    CustomUser,
    Student,
    Teacher,
    Parent
)
from classroom.models import (
    Classroom,
)


# Import input forms to use
from .forms import (
    CustomUserCreationForm,
    StudentCreationForm,
    TeacherCreationForm,
    ParentCreationForm
)

# Import decorators for authorization and checks
from .decorators import unauthenticated_user

# CREATE VIEWS HERE
# ------------------------------------------------------------------------------

'''
TODO: When user registers, need to make sure they get added to the 
correct group.  This can be done later with Django signals.
For now, we will query a group and attach a user to that group.
'''
# Define logic for registration page.
@unauthenticated_user
def student_register(request):
    # Render form that we will modify.
    form = StudentCreationForm()
    # If POST request, handle the request with the provided form.
    if request.method == 'POST':
        # Use POST data to render a form.
        form = StudentCreationForm(request.POST)
        # Validate form.
        # https://docs.djangoproject.com/en/3.1/ref/forms/validation/
        if form.is_valid():
            # If form is valid, go ahead and save form.
            # When saving form, the user will be created.
            # Django's user creation form should handle pass hashing, making sure no other user has that name, etc
            
            # Save form containing user data.  Calls custom save() on form
            user = form.save()
            # Cleaning helps django digest data from http
            username = form.cleaned_data.get('username')
            
            # Flash message is a way to send a one time to the template
            # https://docs.djangoproject.com/en/3.0/ref/contrib/messages/#using-messages-in-views-and-templates
            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
			
    context = {'form':form}
    return render(request, 'registration/register_student.html', context)


# Define logic for registration page.
@unauthenticated_user
def teacher_register(request):
    # Render form that we will modify.
    form = TeacherCreationForm()
    # If POST request, handle the request with the provided form.
    if request.method == 'POST':
        # Use POST data to render a form.
        form = TeacherCreationForm(request.POST)
        # Validate form.
        # https://docs.djangoproject.com/en/3.1/ref/forms/validation/
        if form.is_valid():
            # Save form containing user data.  Calls custom save() on form
            user = form.save()
            # Cleaning helps django digest data from http
            username = form.cleaned_data.get('username')
            # Query and get the group.  Auto assign to student for now.
            #group = Group.objects.get(name='Teacher')
            # Add group instance to the user.
            #user.groups.add(group)
            # Flash message is a way to send a one time to the template
            # https://docs.djangoproject.com/en/3.0/ref/contrib/messages/#using-messages-in-views-and-templates
            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
			
    context = {'form':form}
    return render(request, 'registration/register_teacher.html', context)


# Define logic for registration page.
@unauthenticated_user
def parent_register(request):
    # Render form that we will modify.
    form = ParentCreationForm()
    # If POST request, handle the request with the provided form.
    if request.method == 'POST':
        # Use POST data to render a form.
        form = ParentCreationForm(request.POST)
        # Validate form.
        # https://docs.djangoproject.com/en/3.1/ref/forms/validation/
        if form.is_valid():
            # Save form containing user data.  Calls custom save() on form
            user = form.save()
            # Cleaning helps django digest data from http
            username = form.cleaned_data.get('username')
            # Query and get the group.  Auto assign to student for now.
            #group = Group.objects.get(name='Parent')
            # Add group instance to the user.
            #user.groups.add(group)
            # Flash message is a way to send a one time to the template
            # https://docs.djangoproject.com/en/3.0/ref/contrib/messages/#using-messages-in-views-and-templates
            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
			
    context = {'form':form}
    return render(request, 'registration/register_parent.html', context)



# Define logic for login page.
@unauthenticated_user
def loginPage(request):
    if request.method == 'POST':
        # Get data from input fields in login template
        username = request.POST.get('username')
        password =request.POST.get('password')
        # Make sure user is actually in the user database
        user = authenticate(request, username=username, password=password)
        # If user is authenticated, direct them to the home page
        if user is not None:
            # Login the user
            login(request, user)
            # Redirect the user based on the user type.
            if user.is_student:
                # Get the student object and see if there is a
                # classroom for the student to go to.
                class_to_attend = Student.objects.get(user=user).stud_class
                if class_to_attend:
                    return redirect('home')
                else:
                    return redirect('no_associated_class')
            elif user.is_parent:
                return redirect('parent_portal')
            elif user.is_teacher:
                class_to_manage = Classroom.objects.get(teacher__user=user)
                if class_to_manage:
                    return redirect('teacher_portal')
                else:
                    return redirect('no_associated_class')
            else:
                messages.info(request, 'Account authenticated, but no user role associated with account.')
        else:
            # Using messages module to notify of nonexistent user account.
            messages.info(request, 'Username OR password is incorrect')
    
    context = {}
    return render(request, 'registration/login.html', context)


# Define user logout logic
def logoutUser(request):
	# Just hand off request to logout method.
    logout(request)
    return redirect('login')






    # class student_register(CreateView):
#     model = CustomUser
#     form_class = StudentCreationForm
#     template_name = 'registration/register_student.html'

#     def get_success_url(self):
#         messages.success(request, 'Account was created.')
#         return reverse_lazy('home')

    # def validation(self, form):
    #     # Validate form.
    #     # https://docs.djangoproject.com/en/3.1/ref/forms/validation/
    #     if form.is_valid():
    #         # If form is valid, go ahead and save form.
    #         # When saving form, the user will be created.
    #         # Django's user creation form should handle pass hashing, making sure no other user has that name, etc
            
    #         # Just make user equal to the saved form.
    #         user = form.save()
    #         # Query and get the group.  Auto assign to student for now.
    #         group = Group.objects.get(name='Student')
    #         # Add group instance to the user.
    #         user.groups.add(group)
    #         # Flash message is a way to send a one time to the template
    #         # https://docs.djangoproject.com/en/3.0/ref/contrib/messages/#using-messages-in-views-and-templates
    #         messages.success(request, 'Account was created for ' + username)
    #         # Auto login the user and redirect to home page
    #         login(self.request, user)
    #         return redirect('home')