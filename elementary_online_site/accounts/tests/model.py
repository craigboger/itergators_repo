# accounts/tests/model.py

'''
This is mainly for API testing where we create, update, or delete
a model and need to test model state throughout that process.
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse

# Model Imports
from django.contrib.auth import get_user_model

# TEST IMPLEMENTATION
# ------------------------------------------------------------------------------


class UserModelAndRolesTest(TestCase):
    '''
    Test to make sure the user models function upon creation and will
    authenticate when logging in.
    '''
    @classmethod
    def setUpTestData(cls):
        # Create student user
        cls.user_student = get_user_model().objects.create_user(
            username = 'testStudent',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'TestStudentFirstName',
            last_name = 'TestStudentLastName',
            is_student = True
        )
        cls.user_student.save()
        # Create teacher user
        cls.user_teacher = get_user_model().objects.create_user(
            username = 'testTeacher',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'TestTeacherFirstName',
            last_name = 'TestTeacherLastName',
            is_teacher = True
        )
        cls.user_teacher.save()

    def setUp(self):
        self.student_login_credentials = {
            'username': 'testStudent',
            'password': 'testPass123'
        }
    
    def tearDown(self):
        print("Tearing down Login Redirection Test")
        

    def test_correct_user_login(self):
        '''
        Test a normal user can login and be authenticated.
        '''
        user = self.client.login(username='testStudent', password='testPass123')
        self.assertTrue(user)

    def test_incorrect_user_login(self):
        '''
        Test that an invalid user cannot login
        '''
        user = self.client.login(username='BadPerson', password='BadPassword')
        self.assertFalse(user)
