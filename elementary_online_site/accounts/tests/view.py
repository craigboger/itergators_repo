# accounts/tests/view.py

'''
This is mainly for route testing to ensure that when a user goes to 
a link or there is a redirect, it goes to the correct location.
'''

# IMPORTS
# ------------------------------------------------------------------------------

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Model Imports
from django.contrib.auth import get_user_model
from accounts.models import (
    CustomUser,
    Student,
    Teacher,
)
from classroom.models import (
    Classroom,
    WeeklyClassroomContent,
    SubjectPage,
    WeeklySubjectContent
)

# TEST IMPLEMENTATION
# ------------------------------------------------------------------------------

'''
Simple tests for making sure pages load
'''

class LoginPageViewTest(TestCase):
    '''
    Test to make sure login page loads and is at correct location.
    '''
    def setUp(self):
        pass
    
    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/accounts/login/')
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('login'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('login'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'registration/login.html')


class LogoutPageViewTest(TestCase):
    '''
    Test to make sure the logout route exists and is successful.
    '''
    def setUp(self):
        pass
    
    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/accounts/logout/')
        self.assertEqual(resp.status_code, HTTPStatus.FOUND)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('logout'))
        self.assertEqual(resp.status_code, HTTPStatus.FOUND)


class StudentRegistrationPageViewTest(TestCase):
    '''
    Test to make sure the student registration page loads and is at the
    correct location.
    '''
    def setUp(self):
        pass
    
    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/accounts/register_student/')
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('register_student'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('register_student'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'registration/register_student.html')


class TeacherRegistrationPageViewTest(TestCase):
    '''
    Test to make sure the teacher registration page loads and is at the
    correct location.
    '''
    def setUp(self):
        pass
    
    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/accounts/register_teacher/')
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('register_teacher'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('register_teacher'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'registration/register_teacher.html')


class ParentRegistrationPageViewTest(TestCase):
    '''
    Test to make sure the parent registration page loads and is at the
    correct location.
    '''
    def setUp(self):
        pass
    
    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/accounts/register_parent/')
        self.assertEqual(resp.status_code, HTTPStatus.OK)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('register_parent'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
    
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('register_parent'))
        self.assertEqual(resp.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(resp, 'registration/register_parent.html')


class LoginPageRedirectionTest(TestCase):
    '''
    Class to test that login page redirects each user type to the
    appropriate page.
    '''

    @classmethod
    def setUpTestData(cls):
        # Create teacher user
        cls.user_teacher = get_user_model().objects.create_user(
            username = 'testTeacher',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'TestTeacherFirstName',
            last_name = 'TestTeacherLastName',
            is_teacher = True
        )
        # Create teacher entry in the database
        cls.test_teacher = Teacher.objects.create(
            user=cls.user_teacher
        )
        cls.test_teacher.save()
        # Create classroom that teacher oversees and student can be a part of.
        cls.test_classroom = Classroom.objects.create(
            name='Test Classroom',
            current_week=1,
            teacher=cls.test_teacher
        )
        cls.test_classroom.save()
        # Create weekly content to attach to the new classroom
        cls.test_weekly_class_content = WeeklyClassroomContent.objects.create(
            week_number=1,
            week_date_header='Welcome to week 1',
            whiteboard_link='https://www.google.com',
            source_class=cls.test_classroom
        )
        cls.test_weekly_class_content.save()
        # Create student user and add student to the class
        cls.user_student = get_user_model().objects.create_user(
            username = 'testStudent',
            email = 'test@mail.com',
            password = 'testPass123',
            first_name = 'TestStudentFirstName',
            last_name = 'TestStudentLastName',
            is_student = True
        )
        cls.user_student.save()
        # Create student entry in database
        cls.test_student = Student.objects.create(
            user=cls.user_student,
            stud_class=cls.test_classroom
        )
        cls.test_student.save()

    def setUp(self):
        # Login credentials to test successful login
        self.student_login_credentials = {
            'username': 'testStudent',
            'password': 'testPass123'
        }
        self.teacher_login_credentials = {
            'username': 'testTeacher',
            'password': 'testPass123'
        }
        
    def test_redirect_if_not_logged_in(self):
        # Test to make sure if someone isn't logged in, they are directed to the 
        # login page.
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND) # Check successful loading of page
        self.assertRedirects(response, '/accounts/login/?next=/')   # Check correct URL was redirected to 

    def test_student_user_redirect(self):
        '''
        Test to make sure that a student logging in is redirected
        to the appropriate classroom.
        '''
        # POST to login page to login user and see if authenticated.
        response = self.client.post(reverse_lazy('login'), self.student_login_credentials, follow=True) # Post login of student to login page
        self.assertTrue(response.context['user'].is_authenticated)  # Check student was authenticated
        self.assertRedirects(response, '/class_wk_1/')

    def test_teacher_user_redirect(self):
        '''
        Test to make sure that a teacher logging in is redirected to the 
        teacher admin page.
        '''
        response = self.client.post(reverse_lazy('login'), self.teacher_login_credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertRedirects(response, '/teacher_portal/admin_class_wk_1/')
   