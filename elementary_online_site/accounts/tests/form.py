# accounts/tests/form.py

'''
This is mainly for unit testing of forms and main admin functionality
of the website.  

Mainly for ensuring the front end functions how it is advertised.
'''

# Simple Testing
from django.test import TestCase

# URL utilities
from django.urls import reverse
from django.urls import reverse_lazy
from http import HTTPStatus

# Models
from django.contrib.auth import get_user_model
from accounts.models import (
    Student,
    Teacher
)

class StudentRegistrationTest(TestCase):
    '''
    Class to test the student registration form on the registration
    page functions.
    '''

    @classmethod
    def setUpTestData(cls):
        # Generate student user data
        cls.username = 'testStudent'
        cls.password1 = 'testPass123'
        cls.password2 = 'testPass123'
        cls.first_name = 'Student'
        cls.last_name = 'Person'
        cls.birth_date = '2012-11-29'
        # Generate guardian info
        cls.guardian_1_first_name = 'Guard1First'
        cls.guardian_1_last_name = 'Guard1Last'
        cls.guardian_1_email = 'guard1@mail.com'
        cls.guardian_1_phone = '+12125552368'
        cls.guardian_2_first_name = 'Guard2First'
        cls.guardian_2_last_name = 'Guard2Last'
        cls.guardian_2_email = 'guard2@mail.com'
        cls.guardian_2_phone = '+12125552368'

    def tearDown(self):
        # Delete the user and student from the database
        saved_user = get_user_model().objects.get(username=self.username)
        saved_student = Student.objects.get(user__id=saved_user.id)
        saved_user.delete()
        saved_student.delete()

    def test_student_registration_with_all_fields(self):
        response = self.client.post(
            "/accounts/register_student/", 
            follow=True,
            data={
                'username': self.username,
                'password1': self.password1,
                'password2': self.password2,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'birth_date': self.birth_date,
                'guardian_1_first_name': self.guardian_1_first_name,
                'guardian_1_last_name': self.guardian_1_last_name,
                'guardian_1_email': self.guardian_1_email,
                'guardian_1_phone': self.guardian_1_phone,
                'guardian_2_first_name': self.guardian_2_first_name,
                'guardian_2_last_name': self.guardian_2_last_name,
                'guardian_2_email': self.guardian_2_email,
                'guardian_2_phone': self.guardian_2_phone,
            }
        )
        # Make sure no redirect errors and we are directed back to the login page.
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertRedirects(response, '/accounts/login/')
        # Make sure the user exists in the database after registering
        saved_user = get_user_model().objects.get(username=self.username)
        saved_student = Student.objects.get(user__id=saved_user.id)
        user_exists = False
        if saved_user and saved_student:
            user_exists = True
        self.assertTrue(user_exists)
        # Make sure all user fields match
        self.assertEqual(saved_user.username, self.username)
        self.assertEqual(saved_user.first_name, self.first_name)
        self.assertEqual(saved_user.last_name, self.last_name)
        #TODO: Fix birthdate assertion
        #self.assertEqual(str(saved_student.birth_date), self.birth_date)
        self.assertEqual(saved_student.guardian1_firstname, self.guardian_1_first_name)
        self.assertEqual(saved_student.guardian1_lastname, self.guardian_1_last_name)
        self.assertEqual(saved_student.guardian1_email, self.guardian_1_email)
        self.assertEqual(saved_student.guardian1_phone, self.guardian_1_phone)
        self.assertEqual(saved_student.guardian2_firstname, self.guardian_2_first_name)
        self.assertEqual(saved_student.guardian2_lastname, self.guardian_2_last_name)
        self.assertEqual(saved_student.guardian2_email, self.guardian_2_email)
        self.assertEqual(saved_student.guardian2_phone, self.guardian_2_phone)