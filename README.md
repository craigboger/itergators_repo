# IterGators - Digital Elementary

[![Game Overview Image](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/itergators_classroom_screenshot.png)]()

As a result of the COVID-19 pandemic, many elementary-aged children are now required to take part in remote learning.  While there exists online learning platforms, like CANVAS, these are usually geared towards students that are older and more comfortable with online platforms.  There are very few platforms geared specifically towards younger students with a simple interface that mimics the classroom.

The goal of Digital Elementary is to make joining Zoom class meetings direct and easy, create a simplified classroom that students can go to for announcements, and subjects associated with that classroom to serve subject-specific content and assignments.

Below is the final list of features that were implemented.  While many more features were envisioned, these are what were implemented due to time constraints.

Classroom:

- Class and subject week selection
- Class and subject week-specific whiteboards
- Zoom meetings with status indicators
- Dynamic list of subjects associated with a classroom
- Assignments associated with each week of a subject
- Per-subject report card with all student submitted assignments

Teacher Admin Portal:

- Selection of class or subject week to administer
- Student list with emergency contact info
- Creation of class and subject week
- Updating of class and subject content
- Setting current week of class
- Assignment CRUD API
- Grading queue
- Student report card with ability to update grades and feedback

Admin Portal:

- Manage user roles and permissions
- Create classroom and subjects
- Associate teacher with a classroom
- Enroll a student in a classroom

---
## Table of Contents

- Project Structure
- Live Site Link
- System Requirements and Deployment
    - Python Environment Setup
    - List of APIs
    - Config / settings.py details
    - Environment Variables (Details)
- Usage
    - Classroom (Student)
    - Teacher Admin Portal (Teacher)
    - Admin Portal (IT Admin)
- Team
- License

---
## Django Project Structure

The Django project consists of a main project directory that contains the project applications and data.  This is where a manage.py script is located.  It is responsible for running the server, managing data, and overall accessing project functionality.  

Within this project folder is the project folder which hosts the settings.py file that contains the configuration of the project.  This might include the root URL routing, all the applications used, database configuration, and static file configurations.

The main project directory also contains the applications served by the project.  In this case, there are 3 applications plus 1, the parent portal, which was placed on the back burner to focus on main site functionality.  The 3 applications are:

- Accounts
    - Responsible for user data management, login system, registration of students, redirecting users based on their role, and page access restriction.
- Classroom
    - Responsible for serving the classroom and subject pages to the students along with their assignments.
- Teacher Portal
    - Responsible for providing an interface to teachers to modify classroom and subject page content.
  
The main project directory also includes folders for templates of the HTML written using the Django template language along with a static folder for CSS, images, JS, and other static content served on the website.  

Below is the broken down folder structure:

- elementary_online_site (main project directory)
  - elementary_online_project (project folder)
      - accounts (accounts application)
        - tests (testing module for accounts application)
      - classroom (classroom application)
        - tests (testing module for classroom application)
      - teacher_portal (teacher portal application)
        - tests (testing module for teacher portal application)
      - templates (HTML templates for all applications)
      - static (static files served by all applications)
      - media (folder where uploaded files are stored)
  
Keep in mind that static and media are not used when the site is deployed.  Those folders are hosted on S3 and the deployed site accesses S3 instead of the local project folders.

---
## Live Site Link

The Digital Elementary site was deployed using Heroku to the below link.  

[Live Digital Elementary Site](https://itergators-digital-elementary.herokuapp.com/)

The static and uploaded files are serviced by an AWS S3 bucket.  The database handling all site data is served from a Postgres server hosted with AWS RDS.  For security, the user data and secret keys accessing those AWS resources are not posted here and are included in a separate report.

---
## System Requirements and Deployment
### Python Environment Setup
Python version: 3.8.5

Packages required in Python environment:

- asgiref==3.2.10
- boto3==1.16.0
- botocore==1.19.30
- Django==3.1.4
- django-bootstrap-modal-forms==2.0.1
- django-phonenumber-field==5.0.0
- django-storages==1.8
- gunicorn==20.0.4
- jmespath==0.10.0
- phonenumbers==8.12.14
- Pillow==8.0.1
- psycopg2==2.8.5
- python-dateutil==2.8.1
- pytz==2020.4
- s3transfer==0.3.3
- six==1.15.0
- sqlparse==0.4.1
- urllib3==1.26.2
- whitenoise==5.2.0
- dj-database-url==0.5.0

All dependencies above except for Python 3.8.5 can be installed with `pip install` with your preferred method of creating a python environment.  Alternatively, the requirements.txt file can be used to generate the environment with the appropriate packages.

### List of APIs
Most APIs are CRUD type APIs for tasks like:

- AWW Whiteboard sharing and embed API.
- Creating, Updating, and Deleting assignments associated with a week in a particular subject.
- Creating and Updating a week of classes and subjects.
- Initial grading and grade update.
- django-bootstrap-modal-forms package.  It provides classes and views a Django project can inherit from to generate templates for modal pop-ups on a Django site.

Other project tasks that could be considered APIs include:

- Interfacing postgres database with Django's model system using psycopg2.
- Using Django's template language with the template engine to create dynamic pages.
- Using Django's user authentication system to restrict page access and serve custom user data.

### Config / settings.py Details

The settings.py file in the project folder contains the main config data for the site including the root URL routing, all the applications used, database configuration, and static file configurations.

In the deployed version of the site, database configuration is setup to use a Postgres database on Amazon's AWS RDS.  If further development is of interest, the project can be configured to use a local SQLite database for development (if no environment variables designating authentication information are available).  Please refer to Django documentation for setting up this configuration.

Similar can be done for the static and media files.  Right now the live project is configured to access an AWS S3 bucket to store uploaded files and serve static files.  This can also be reconfigured to use the local static and media folders for further development if there are no environment variables available to provide the S3 bucket ID and secret key.

Below is a list of settings in the order they appear in the settings.py script.  Please refer to the Django documentation and embedded links for details on manipulating the settings.

Settings list:

- Security 
- Applications
- Middleware
- URLS
- WSGI Application to use
- Templates config
- Database config
- Password restrictions and validators
- Internationalization settings
- Static and media files config
- User model to use
- Login settings

### Environment Variables

Environment variables expected to be provided by Heroku or other Python environment include:

- AWS_ACCESS_KEY_ID
    - This is the ID of the user account used in AWS's IAM user manager that provides access to the S3 bucket for storing uploaded files and serving static files.
- AWS_SECRET_ACCESS_KEY
    - The access key to the account specified by the AWS_ACCESS_KEY_ID used in AWS's IAM user manager that provides access to the S3 bucket for storing uploaded files and serving static files.
- DATABASE_URL
    - Postgres database link that has the username, password, and name of the database on the server Django will use to store user, class, subject, and assignment data.
- DJANGO_SECRET_KEY 
    - Secret key used by Django to create signed data or generate unique salt for hash functions.  
  

  If you want to take the project and run your own version, you will need to:
1. Configure use of SQLite database.
2. Configure local static and media directories.
3. Generate new Django secret key.

For further details on running a Django project, refer to the Django documentation.

---
## Usage

### Authentication

When a user logs in as a student, they are directed to the appropriate classroom or teacher portal.  

![Student Login GIF](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/student_logging_in_anim.gif)

### Classroom

#### Classroom

The classroom contains a general whiteboard for announcements and class content, links to Zoom meeeting rooms, status for those Zoom meeting rooms, along with buttons to all subject pages associated with the classroom.

![Classroom Nav GIF](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/classroom_nav_anim.gif)

#### Subject Page

The subject page contains a whiteboard for subject-specific content for the week, assignments to do, and linking to the report card.

![Subject Page Nav GIF](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/subject_page_nav_anim.gif)

#### Report Card

The report card contains all the submitted assignments from the student for that subject.  Students can update their submissions if they need to make corrections.  When an assignment is graded, the submitted assignment will turn green and display the grade.  Clicking on the assignment reveals the grade and feedback.

![Report Card Nav](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/report_card_navigation.gif)

### Teacher Admin Portal

#### Classroom Admin

The classroom admin age includes cards for managing:

- Weekly classroom content (page title and whiteboard)
- General classroom content (zoom links, availability, and currently set week)
- Creating new week of class and subjects
- Student list with emergency contact info

![Teacher Class Admin Nav](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/teacher_class_admin_anim.gif)

#### Subject Page Admin

The subject admin page includes cards for managing:

- Weekly subject page content and assignments
- A student list for the subject with links to student report cards
- Grading queue of submitted assignments that need to be graded

The teacher can grade submitted assignments either from the student report card or the grading queue.

![Teacher Subject Page Admin Nav](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/teacher_subject_page_admin_anim.gif)

### Admin Portal

The admin portal is a modified version of the Django admin system that allows for:

- Managing users and their roles
- Enrolling students into classes
- Creating classrooms and associating them with a teacher
- Creating subjects associated with a classroom

![Django Admin Page Nav](https://uf-cen3031-itergators-static-and-uploads2.s3.us-east-2.amazonaws.com/readme_images/django_admin_nav.gif)

---
## Team
The IterGators formed as a result of the "CEN3031 - Software Engineering" 
course at the University of Florida developed by [Dr. Sanethia Thomas](https://www.cise.ufl.edu/thomas-sanethia/)

Team members include:

- Alina Mitchell [Team Lead]
- Craig Boger [Scrum Master]
- Malyssa Calarco [Frontend Developer]
- Abraham Yang [Frontend Developer]

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2020 ©